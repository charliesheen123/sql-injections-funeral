﻿using SQLInjectionsFuneral.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace SQLInjectionsFuneral.Migrator
{
    /// <summary>
    /// Info about model class in order to create table
    /// </summary>
    internal class TableType
    {
        internal string Name { get; set; }
        internal Type Type { get; set; }
        internal IEnumerable<ColumnInfo> ColumnInfos { get; set; }
        internal bool IsDerived { get; set; }
    }
}
