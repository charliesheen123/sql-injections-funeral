﻿using SQLInjectionsFuneral.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace SQLInjectionsFuneral.Migrator
{
    /// <summary>
    /// Wrapper class for relationships
    /// </summary>
    internal class FKWrapper
    {
        public TableType SrcTable { get; set; }
        public List<FKReference> References { get; set; }
    }

    /// <summary>
    /// Foreign key informations
    /// </summary>
    internal class FKReference
    {
        public TableType ReferencedTable { get; set; }
        public string FKColumn { get; set; }
        public bool IsCollection { get; set; }
        public bool IsCreated { get; set; }
    }
}
