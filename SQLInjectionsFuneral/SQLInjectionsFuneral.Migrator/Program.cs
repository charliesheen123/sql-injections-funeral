﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SQLInjectionsFuneral.Migrator
{
    public class Program
    {
        public async static Task Main(string[] args)
        {
            try
            {
                var cmd = args[0];

                if (cmd == "create")
                {
                    var dll = args[1];
                    var connStr = args[2];

                    var asm = Assembly.LoadFrom(dll);
                    var _dbCreator = new DBCreator(asm, connStr);

                    if (!_dbCreator.HasContext())
                    {
                        Console.Error.WriteLine("Error: No context class found in assembly.");
                        return;
                    }

                    if (_dbCreator.HasMultipleContexts())
                    {
                        Console.Error.WriteLine("Error: Multiple context classes detected in assembly. Only one is supported.");
                        return;
                    }

                    await _dbCreator.Initialize();
                    var tableCount = await _dbCreator.CreateTables();

                    Console.WriteLine($"{tableCount} tables created successfully.");
                }
                else if (new[] { "help", "-h", "--help", "/h", "/help" }.Contains(cmd.ToLower()))
                {
                    Console.WriteLine("Usage: (create|help) <assemblyPath> <connectionString>");
                }
                else
                {
                    Console.Error.WriteLine($"Unknown command: {cmd}");
                }
            }
            catch (IndexOutOfRangeException)
            {
                Console.Error.WriteLine("Error: Invalid command parameters.");
            }
            catch (IOException)
            {
                Console.Error.Write($"Error: Invalid assembly path: {args[1]}");
            }
        }
    }
}
