﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Sources;
using SQLInjectionsFuneral.Attributes;
using SQLInjectionsFuneral.Exceptions;
using SQLInjectionsFuneral.Metadata;
using KeyAttribute = SQLInjectionsFuneral.Attributes.KeyAttribute;

namespace SQLInjectionsFuneral.Migrator
{
    /// <summary>
    /// Class for creating the DB tables
    /// </summary>
    internal class DBCreator : IDisposable
    {
        private readonly Assembly _asm;
        private readonly SqlConnection _sql;
        private readonly List<TableType> _tableTypes;

        //private Context _ctx;
        private readonly List<FKWrapper> _pendingFKs = new List<FKWrapper>();

        internal DBCreator(Assembly asm, string connStr)
        {
            _asm = asm;
            _sql = new SqlConnection(connStr);
            _tableTypes = new List<TableType>();
        }

        /// <summary>
        /// Returns true if passed Assembly has a Context class
        /// </summary>
        /// <returns></returns>
        internal bool HasContext()
        {
            return _asm.GetTypes().Any(t => t.BaseType == typeof(Context));
        }

        /// <summary>
        /// Returns true if multiple Context classes have been defined (illegal)
        /// </summary>
        /// <returns></returns>
        internal bool HasMultipleContexts()
        {
            return _asm.GetTypes().Count(t => t.BaseType == typeof(Context)) > 1;
        }
        
        /// <summary>
        /// Initialize the DB connection
        /// </summary>
        /// <returns></returns>
        internal async Task Initialize()
        {
            var ctxType = _asm.GetTypes().Single(t => t.BaseType == typeof(Context));

            var tableProps = ctxType.GetProperties().Where(p => p.PropertyType.GetGenericTypeDefinition() == typeof(Table<>));
            foreach (var t in tableProps)
            {
                var type = t.PropertyType.GenericTypeArguments[0];
                _tableTypes.Add(new TableType
                {
                    Name = t.Name,
                    Type = type,
                    IsDerived = type.BaseType != typeof(object)
                });
            }

            await _sql.OpenAsync();
        }

        /// <summary>
        /// Create the tables
        /// </summary>
        /// <returns></returns>
        internal async Task<int> CreateTables()
        {
            int i = 0;
            await DropTables();
            foreach (var t in _tableTypes)
            {
                t.ColumnInfos = LoadColumnInfos(t.Type);
                await CreateTable(t);

                i++;
            }

            await CreateForeignKeys();

            return i;
        }

        /// <summary>
        /// Load the infos for column definition for the model type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private IEnumerable<ColumnInfo> LoadColumnInfos(Type type)
        {
            var colInfos = new List<ColumnInfo>();
            var hasOneKey = false;
            var identityColCount = 0;
            var props = type.GetProperties().Where(pi => !pi.CustomAttributes.Any(a => a.AttributeType == typeof(IgnoredAttribute)));

            foreach (var pi in props)
            {
                if (!IsMappable(pi)) throw new NotSupportedException($"The Type '{pi.PropertyType.FullName}' is not mappable.");
                if (!IsVirtual(pi)) throw new NotSupportedException($"The Property '{pi.Name}' has to be declared as VIRTUAL to be mapped.");
            }

            var columnProps = props.Where(pi => Mappings.Contain(pi.PropertyType));
            var navigationProps = props.Where(pi => IsNavigationProp(pi));

            foreach (var pi in columnProps)
            {
                var hasNotNullAttr = pi.CustomAttributes.Any(a => a.AttributeType == typeof(NotNullAttribute));

                IdentityAttribute identityAttr = null;
                var identityAttrType = pi.CustomAttributes.FirstOrDefault(a => a.AttributeType == typeof(IdentityAttribute))?.AttributeType;
                if (identityAttrType != null)
                {
                    identityColCount++;
                    identityAttr = pi.GetCustomAttribute(identityAttrType) as IdentityAttribute;
                }

                var colInfo = new ColumnInfo
                {
                    Name = pi.Name,
                    Type = Mappings.TypeMappings[pi.PropertyType],
                    ForeignKeyPropName = null,
                    IsNavigation = false,
                    IsNullable = !hasNotNullAttr && ((pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)) || !pi.PropertyType.IsValueType),
                    IsInherited = pi.DeclaringType != type,
                    IsUnique = pi.CustomAttributes.Any(a => a.AttributeType == typeof(UniqueAttribute)),
                    IsIdentity = identityAttr != null,
                    IdentitySeed = identityAttr?.Seed,
                    IdentityIncrement = identityAttr?.Increment
                };

                if (pi.CustomAttributes.Count(a => a.AttributeType == typeof(KeyAttribute)) == 1)
                {
                    hasOneKey = true;
                    colInfo.IsKey = true;
                }
                else
                {
                    colInfo.IsKey = false;
                }

                colInfos.Add(colInfo);
            }

            foreach (var pi in navigationProps)
            {
                var isCollection = pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition() == typeof(ICollection<>);

                if (pi.CustomAttributes.Any(a => a.AttributeType == typeof(KeyAttribute)))
                    throw new NotSupportedException($"The Type '{pi.PropertyType.FullName}' is invalid for primary key usage.");

                if (!isCollection && !pi.CustomAttributes.Any(a => a.AttributeType == typeof(ForeignKeyAttribute)))
                    throw new EntityException($"Navigation properties must be decorated with a {typeof(ForeignKeyAttribute).FullName}");

                string fkName = null;
                if (!isCollection)
                {
                    var attrType = pi.CustomAttributes.First(a => a.AttributeType == typeof(ForeignKeyAttribute)).AttributeType;
                    var attr = pi.GetCustomAttribute(attrType) as ForeignKeyAttribute;
                    fkName = attr.ForeignKeyName;
                }

                //var fkCol = colInfos.FirstOrDefault(c => c.Name == fkName) ?? throw new EntityException($"Invalid foreign key name: {fkName}");

                colInfos.Add(new ColumnInfo
                {
                    Name = pi.Name,
                    Type = !isCollection ? pi.PropertyType.FullName : pi.PropertyType.GenericTypeArguments[0].FullName,
                    ForeignKeyPropName = fkName,
                    IsKey = false,
                    IsUnique = false,
                    IsNavigation = true,
                    IsCollection = isCollection,
                    IsNullable = false,
                    IsInherited = pi.DeclaringType != type
                });
            }

            if (type.BaseType != typeof(object))
            {
                colInfos.Add(new ColumnInfo
                {
                    IsNavigation = true,
                    Type = type.BaseType.FullName,
                    ForeignKeyPropName = colInfos.First(c => c.IsKey).Name
                });
            }

            if (!hasOneKey) throw new EntityException("Excactly one primary key has to be declared.");
            if (identityColCount > 1) throw new EntityException("Not more than one identity column can be declared.");

            return colInfos;
        }

        /// <summary>
        /// Drop all existing tables
        /// </summary>
        /// <returns></returns>
        private async Task DropTables()
        {
            using var fkSelect = new SqlCommand($"SELECT TABLE_NAME, CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'FOREIGN KEY' AND TABLE_CATALOG = '{_sql.Database}'", _sql);
            using var fkResult = await fkSelect.ExecuteReaderAsync();

            while (await fkResult.ReadAsync())
            {
                var tName = fkResult.GetString(0);
                var fkName = fkResult.GetString(1);

                using var fkCmd = new SqlCommand($"ALTER TABLE {tName} DROP CONSTRAINT {fkName};", _sql);
                await fkCmd.ExecuteNonQueryAsync();
            }


            using var select = new SqlCommand($"SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG = '{_sql.Database}'", _sql);
            using var result = await select.ExecuteReaderAsync();

            while (await result.ReadAsync())
            {
                var name = result.GetString(0);

                using var cmd = new SqlCommand($"IF OBJECT_ID('{name}', 'U') IS NOT NULL DROP TABLE {name};", _sql);
                await cmd.ExecuteNonQueryAsync();
            }
        }

        /// <summary>
        /// Create a table
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private async Task CreateTable(TableType t)
        {
            var cmdStr = $"CREATE TABLE {t.Name} (";

            var fk = new FKWrapper
            {
                SrcTable = t,
                References = new List<FKReference>()
            };

            foreach (var col in t.ColumnInfos.Where(c => c.IsKey || !c.IsInherited))
            {
                if (!col.IsNavigation)
                {
                    if ((col.IsKey || col.IsUnique) && col.Type == "NVARCHAR(MAX)")
                        col.Type = "NVARCHAR(900)";

                    var identityStr = col.IsIdentity ? $"IDENTITY({col.IdentitySeed ?? 1}, {col.IdentityIncrement ?? 1})" : "";
                    var uniqueStr = col.IsUnique ? "UNIQUE" : "";
                    var notNullStr = col.IsNullable && !col.IsKey ? "NULL" : "NOT NULL";

                    cmdStr += $"{col.Name} {col.Type} {identityStr} {uniqueStr} {notNullStr}, ";
                }
                else
                {
                    var referencedTable = _tableTypes.First(tt => tt.Type.FullName == col.Type);
                    var fkRef = fk.References.FirstOrDefault(r => r.ReferencedTable == referencedTable);

                    fk.References.Add(new FKReference
                    {
                        ReferencedTable = referencedTable,
                        FKColumn = col.ForeignKeyPropName,
                        IsCollection = col.IsCollection,
                        IsCreated = false
                    });
                }
            }

            cmdStr += $"PRIMARY KEY ({string.Join(", ", t.ColumnInfos.Where(c => c.IsKey).Select(c => c.Name))}), ";
            cmdStr += ")";

            _pendingFKs.Add(fk);

            using var cmd = new SqlCommand(cmdStr, _sql);
            await cmd.ExecuteNonQueryAsync();
        }

        /// <summary>
        /// Create the foreign key constraints
        /// </summary>
        /// <returns></returns>
        private async Task CreateForeignKeys()
        {
            foreach (var fk in _pendingFKs)
            {
                foreach (var fkRef in fk.References)
                {
                    if (fkRef.IsCollection) continue;

                    var cmdStr = $"ALTER TABLE {fk.SrcTable.Name} ADD FOREIGN KEY ({fkRef.FKColumn}) REFERENCES {fkRef.ReferencedTable.Name};";

                    using var cmd = new SqlCommand(cmdStr, _sql);
                    await cmd.ExecuteNonQueryAsync();
                    fkRef.IsCreated = true;

                    var inverseRef = _pendingFKs.SelectMany(f => f.References).FirstOrDefault(r => r.IsCollection && r.ReferencedTable == fk.SrcTable);
                    if (inverseRef != null)
                        inverseRef.IsCreated = true;
                }
            }

            foreach (var fk in _pendingFKs.OrderBy(f => f.SrcTable.Name))
            {
                foreach (var fkRef in fk.References.OrderBy(f => f.ReferencedTable.Name).Where(r => r.IsCollection && !r.IsCreated))
                {
                    if (fkRef.IsCreated) continue;

                    var key1 = fk.SrcTable.ColumnInfos.First(c => c.IsKey);
                    var key2 = fkRef.ReferencedTable.ColumnInfos.First(c => c.IsKey);

                    var key1Str = $"{fk.SrcTable.Name}_{key1.Name} {key1.Type} NOT NULL REFERENCES {fk.SrcTable.Name}";
                    var key2Str = $"{fkRef.ReferencedTable.Name}_{key2.Name} {key2.Type} NOT NULL REFERENCES {fkRef.ReferencedTable.Name}";
                    var pkStr = $"PRIMARY KEY ({fk.SrcTable.Name}_{key1.Name}, {fkRef.ReferencedTable.Name}_{key2.Name})";

                    var cmdStr = $"CREATE TABLE {fk.SrcTable.Name}{fkRef.ReferencedTable.Name} ({key1Str}, {key2Str}, {pkStr});";
                    using var cmd = new SqlCommand(cmdStr, _sql);
                    await cmd.ExecuteNonQueryAsync();
                    fkRef.IsCreated = true;

                    var inverseRef = _pendingFKs.SelectMany(f => f.References).First(r => r.IsCollection && !r.IsCreated && r.ReferencedTable == fk.SrcTable);
                    if (inverseRef != null)
                        inverseRef.IsCreated = true;
                }
            }
        }

        /// <summary>
        /// True, if a property is declared as virtual
        /// </summary>
        /// <param name="pi"></param>
        /// <returns></returns>
        private bool IsVirtual(PropertyInfo pi)
        {
            return pi.GetSetMethod().IsVirtual;
        }

        /// <summary>
        /// True if a property can be mapped
        /// </summary>
        /// <param name="pi"></param>
        /// <returns></returns>
        private bool IsMappable(PropertyInfo pi)
        {
            return Mappings.Contain(pi.PropertyType) || IsNavigationProp(pi);
        }

        /// <summary>
        /// True if a property is a navigation property
        /// </summary>
        /// <param name="pi"></param>
        /// <returns></returns>
        private bool IsNavigationProp(PropertyInfo pi)
        {
            return pi.GetGetMethod().IsVirtual && pi.GetSetMethod().IsVirtual
                && _tableTypes.Any(t => t.Type == pi.PropertyType || (pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition() == typeof(ICollection<>) && t.Type == pi.PropertyType.GenericTypeArguments[0]));
        }

        #region IDisposable
        private bool disposedValue;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _sql.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~DBCreator()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
