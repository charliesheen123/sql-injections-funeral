﻿using NuGet.Frameworks;
using NUnit.Framework;
using SampleModels;
using SQLInjectionsFuneral.Migrator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLInjectionsFuneral.Tests
{
    /// <summary>
    /// Insert, Update, Delete tests for models with multiple relationships
    /// </summary>
    public class ComplexDMLTests
    {
        private SampleContext _db;

        [SetUp]
        public async Task Setup()
        {
            _db = new SampleContext(Configuration.CONN_STR);

            await Program.Main(new[] { "create", Configuration.DLL_PATH, Configuration.CONN_STR });
        }


        #region Insert
        [Test]
        public async Task TestInheritanceInsert()
        {
            var corey = new American
            {
                FirstName = "Corey",
                LastName = "Taylor",
                BirthDate = new DateTime(1973, 12, 8),
                HomeState = "IA",
                ImmigrationDate = null
            };
            _db.Americans.Add(ref corey);
            await _db.SaveChangesAsync();

            Assert.AreEqual(1, _db.People.Count());
            Assert.AreEqual(1, _db.Americans.Count());
        }

        [Test]
        public async Task Test1toNInsert()
        {
            var desMoines = new City
            {
                Name = "Des Moines",
                State = "Iowa (IA)",
                Country = "USA",
                Population = 215000
            };
            _db.Cities.Add(ref desMoines);

            var corey = new American
            {
                FirstName = "Corey",
                LastName = "Taylor",
                BirthDate = new DateTime(1973, 12, 8),
                City = desMoines,
                HomeState = "IA",
                ImmigrationDate = null
            };
            _db.Americans.Add(ref corey);
            await _db.SaveChangesAsync();

            Assert.AreEqual(desMoines.CityId, corey.CityId);
            Assert.AreEqual(desMoines, corey.City);
        }

        [Test]
        public async Task TestInverse1toNInsert()
        {
            var corey = new American
            {
                FirstName = "Corey",
                LastName = "Taylor",
                BirthDate = new DateTime(1973, 12, 8),
                HomeState = "IA",
                ImmigrationDate = new DateTime(1973, 12, 8)
            };
            _db.Americans.Add(ref corey);
            await _db.SaveChangesAsync();

            var desMoines = new City
            {
                Name = "Des Moines",
                State = "Iowa (IA)",
                Country = "USA",
                Population = 215000
            };
            desMoines.People.Add(corey);

            _db.Cities.Add(ref desMoines);
            await _db.SaveChangesAsync();

            Assert.AreEqual(desMoines.CityId, corey.CityId);
            Assert.AreEqual(desMoines, corey.City);
        }

        [Test]
        public async Task TestMtoNInsert()
        {
            var corey = new American
            {
                FirstName = "Corey",
                LastName = "Taylor",
                BirthDate = new DateTime(1973, 12, 8),
                HomeState = "IA",
                ImmigrationDate = null
            };
            _db.Americans.Add(ref corey);
            await _db.SaveChangesAsync();

            var company = new Company
            {
                Name = "Slipknot",
                BusinessField = "Heavy Metal",
                LegalForm = "Inc."
            };
            company.People.Add(corey);
            _db.Companies.Add(ref company);

            await _db.SaveChangesAsync();
            Assert.AreEqual(1, corey.Companies.Count);
            Assert.AreEqual(1, company.People.Count);
        }
        #endregion

        #region Update
        [Test]
        public async Task TestInheritanceUpdate()
        {
            var corey = new American
            {
                FirstName = "Corey",
                LastName = "Taylor",
                BirthDate = new DateTime(1973, 12, 8),
                HomeState = "IA",
                ImmigrationDate = null
            };
            _db.Americans.Add(ref corey);
            await _db.SaveChangesAsync();

            corey.FirstName += " Todd";
            corey.ImmigrationDate = corey.BirthDate;

            await _db.SaveChangesAsync();

            Assert.AreEqual("Corey Todd", corey.FirstName);
            Assert.AreEqual(corey.ImmigrationDate, corey.BirthDate);
        }

        [Test]
        public async Task Test1toNUpdate()
        {
            var desMoines = new City
            {
                Name = "Des Moines",
                State = "Iowa (IA)",
                Country = "USA",
                Population = 215000
            };
            _db.Cities.Add(ref desMoines);

            var corey = new American
            {
                FirstName = "Corey",
                LastName = "Taylor",
                BirthDate = new DateTime(1973, 12, 8),
                City = desMoines,
                HomeState = "IA",
                ImmigrationDate = null //new DateTime(1973, 12, 8)
            };
            _db.Americans.Add(ref corey);
            await _db.SaveChangesAsync();


            var lasVegas = new City
            {
                Name = "Las Vegas",
                State = "Nevada (NV)",
                Country = "USA",
                Population = 650000
            };
            _db.Cities.Add(ref lasVegas);
            corey.City = lasVegas; 

            await _db.SaveChangesAsync();

            Assert.AreEqual(lasVegas.CityId, corey.CityId);
            Assert.AreEqual(lasVegas, corey.City);
        }

        [Test]
        public async Task TestInverse1toNUpdate()
        {
            var corey = new American
            {
                FirstName = "Corey",
                LastName = "Taylor",
                BirthDate = new DateTime(1973, 12, 8),
                HomeState = "IA",
                ImmigrationDate = null //new DateTime(1973, 12, 8)
            };
            _db.Americans.Add(ref corey);
            await _db.SaveChangesAsync();

            var desMoines = new City
            {
                Name = "Des Moines",
                State = "Iowa (IA)",
                Country = "USA",
                Population = 215000
            };

            desMoines.People.Add(corey);
            _db.Cities.Add(ref desMoines);
            await _db.SaveChangesAsync();


            var lasVegas = new City
            {
                Name = "Las Vegas",
                State = "Nevada (NV)",
                Country = "USA",
                Population = 650000
            };
            desMoines.People.Remove(corey);
            lasVegas.People.Add(corey);
            _db.Cities.Add(ref lasVegas);

            await _db.SaveChangesAsync();

            Assert.AreEqual(lasVegas.CityId, corey.CityId);
            Assert.AreEqual(lasVegas, corey.City);
        }

        [Test]
        public async Task TestMtoNUpdate()
        {
            var corey = new American
            {
                FirstName = "Corey",
                LastName = "Taylor",
                BirthDate = new DateTime(1973, 12, 8),
                HomeState = "IA",
                ImmigrationDate = null
            };
            _db.Americans.Add(ref corey);

            var joey = new American
            {
                FirstName = "Joey",
                LastName = "Jordison",
                BirthDate = new DateTime(1975, 4, 26),
                HomeState = "IA",
                ImmigrationDate = null
            };
            _db.Americans.Add(ref joey);

            var company = new Company
            {
                Name = "Slipknot",
                BusinessField = "Heavy Metal",
                LegalForm = "Inc."
            };
            _db.Companies.Add(ref company);
            await _db.SaveChangesAsync();

            company.People.Add(corey);
            company.People.Add(joey);
            await _db.SaveChangesAsync();

            Assert.AreEqual(2, company.People.Count);
            Assert.AreEqual(1, joey.Companies.Count);

            company.People.Remove(joey);
            await _db.SaveChangesAsync();
            Assert.AreEqual(1, company.People.Count);
            Assert.AreEqual(1, corey.Companies.Count);
            Assert.Zero(joey.Companies.Count);
        }
        #endregion

        #region Delete
        [Test]
        public async Task TestInheritanceDelete()
        {
            var corey = new American
            {
                FirstName = "Corey",
                LastName = "Taylor",
                BirthDate = new DateTime(1973, 12, 8),
                HomeState = "IA",
                ImmigrationDate = null
            };
            _db.Americans.Add(ref corey);
            await _db.SaveChangesAsync();

            _db.Americans.Remove(corey);
            await _db.SaveChangesAsync();

            Assert.Zero(_db.People.Count());
            Assert.Zero(_db.Americans.Count());
        }
        #endregion
    }
}
