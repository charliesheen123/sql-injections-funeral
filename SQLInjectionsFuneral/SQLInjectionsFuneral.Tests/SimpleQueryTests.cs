﻿using NUnit.Framework;
using SampleModels;
using SQLInjectionsFuneral.Extensions;
using SQLInjectionsFuneral.Migrator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLInjectionsFuneral.Tests
{
    /// <summary>
    /// Query tests on models without relationships
    /// </summary>
    public class SimpleQueryTests
    {
        private SampleContext _db;

        [OneTimeSetUp]
        public async Task Setup()
        {
            _db = new SampleContext(Configuration.CONN_STR);

            await Program.Main(new[] { "create", Configuration.DLL_PATH, Configuration.CONN_STR });

            var philly = new City
            {
                Name = "Philadelphia",
                State = "Pennsylvania (PA)",
                Country = "USA",
                Population = 1600000
            };
            _db.Cities.Add(ref philly);

            var boston = new City
            {
                Name = "Boston",
                State = "Massachusetts",
                Country = "USA",
                Population = 700000
            };
            _db.Cities.Add(ref boston);

            var atlanta = new City
            {
                Name = "Atlanta",
                State = "Georgia (GA)",
                Country = "USA",
                Population = 500000
            };
            _db.Cities.Add(ref atlanta);

            await _db.SaveChangesAsync();
        }

        [Test]
        public async Task TestSimpleWhere()
        {
            var threshold = 1000000;

            var res = await _db.Cities.Where(c => c.Population < threshold).ToListAsync();
            Assert.AreEqual(2, res.Count);

            var res2 = await _db.Cities.Where(c => c.Population < 1000000 && c.State == "Georgia (GA)").ToListAsync();
            Assert.AreEqual(1, res2.Count);
        }

        [Test]
        public async Task TestSimpleQueryString()
        {
            var res = await _db.Cities.Where(c => c.Population < 1000000).Select(c => c.Name).ToListAsync();
            Assert.AreEqual(2, res.Count);
            Assert.IsTrue(res is List<string>);
        }

        [Test]
        public async Task TestSimpleQueryInt()
        {
            var res = await _db.Cities.Where(c => c.Population < 1000000).Select(c => c.Population).ToListAsync();
            Assert.AreEqual(2, res.Count);
            Assert.IsTrue(res is List<int>);
        }

        [Test]
        public async Task TestSimpleQueryAnonymous()
        {
            var res = await _db.Cities.Where(c => c.Population < 1000000).Select(c => new { c.Name, c.State }).ToListAsync();
            Assert.AreEqual(2, res.Count);
        }

        [Test]
        public async Task TestDoubleWhere()
        {
            var res = await _db.Cities.Where(c => c.Population < 1000000).Where(c => c.State == "Georgia (GA)").ToListAsync();
            Assert.AreEqual(1, res.Count);
        }

        [Test]
        public async Task TestCount()
        {
            var res = await _db.Cities.CountAsync(c => c.Population < 1000000);
            Assert.AreEqual(2, res);

            var res2 = await _db.Cities.Where(c => c.Population < 1000000).Select(c => c.Name).CountAsync();
            Assert.AreEqual(2, res2);
        }

        [Test]
        public async Task TestSum()
        {
            var res = await _db.Cities.Where(c => c.Population < 1000000).SumAsync(c => c.Population);
            Assert.AreEqual(1200000, res);

            var res2 = await _db.Cities.Where(c => c.Population < 1000000).Select(c => c.Population).SumAsync();
            Assert.AreEqual(1200000, res2);
        }

        [Test]
        public async Task TestMin()
        {
            var res = await _db.Cities.Where(c => c.Population < 1000000).MinAsync(c => c.Population);
            Assert.AreEqual(500000, res);

            var res2 = await _db.Cities.Where(c => c.Population < 1000000).Select(c => c.Population).MinAsync();
            Assert.AreEqual(500000, res2);
        }

        [Test]
        public async Task TestMax()
        {
            var res = await _db.Cities.Where(c => c.Population < 1000000).MaxAsync(c => c.Population);
            Assert.AreEqual(700000, res);

            var res2 = await _db.Cities.Where(c => c.Population < 1000000).Select(c => c.Population).MaxAsync();
            Assert.AreEqual(700000, res2);
        }

        [Test]
        public async Task TestAvg()
        {
            var res = await _db.Cities.Where(c => c.Population < 1000000).AverageAsync(c => c.Population);
            Assert.AreEqual(600000, res);

            var res2 = await _db.Cities.Where(c => c.Population < 1000000).Select(c => c.Population).AverageAsync();
            Assert.AreEqual(600000, res2);
        }

        [Test]
        public async Task TestFirst()
        {
            var res = await _db.Cities.FirstAsync(c => c.Population < 1000000);
            Assert.AreEqual(res.Name, "Boston"); 
            
            var res2 = await _db.Cities.Where(c => c.Population < 1000000).FirstAsync();
            Assert.AreEqual(res2.Name, "Boston");

            var res3 = await _db.Cities.Where(c => c.Population < 1000000).Select(c => c.Name).FirstAsync();
            Assert.AreEqual(res3, "Boston");

            Assert.ThrowsAsync<InvalidOperationException>(() => _db.Cities.FirstAsync(c => c.Population < 100));
        }

        [Test]
        public async Task TestFirstOrDefault()
        {
            var res = await _db.Cities.FirstOrDefaultAsync(c => c.Population < 1000000);
            Assert.AreEqual(res.Name, "Boston");

            var res2 = await _db.Cities.Where(c => c.Population < 1000000).FirstOrDefaultAsync();
            Assert.AreEqual(res2.Name, "Boston");

            var res3 = await _db.Cities.Where(c => c.Population < 1000000).Select(c => c.Name).FirstOrDefaultAsync();
            Assert.AreEqual(res3, "Boston");

            Assert.Null(await _db.Cities.FirstOrDefaultAsync(c => c.Population < 100));
        }

        [Test]
        public async Task TestSingle()
        {
            var res = await _db.Cities.SingleAsync(c => c.Population < 600000);
            Assert.AreEqual(res.Name, "Atlanta");

            var res2 = await _db.Cities.Where(c => c.Population < 600000).SingleAsync();
            Assert.AreEqual(res2.Name, "Atlanta");

            var res3 = await _db.Cities.Where(c => c.Population < 600000).Select(c => c.Name).SingleAsync();
            Assert.AreEqual(res3, "Atlanta");

            Assert.ThrowsAsync<InvalidOperationException>(() => _db.Cities.SingleAsync(c => c.Population < 100));

            Assert.ThrowsAsync<InvalidOperationException>(() => _db.Cities.SingleAsync(c => c.Population < 1000000));
        }

        [Test]
        public async Task TestSingleOrDefault()
        {
            var res = await _db.Cities.SingleOrDefaultAsync(c => c.Population < 600000);
            Assert.AreEqual(res.Name, "Atlanta");

            var res2 = await _db.Cities.Where(c => c.Population < 600000).SingleOrDefaultAsync();
            Assert.AreEqual(res2.Name, "Atlanta");

            var res3 = await _db.Cities.Where(c => c.Population < 600000).Select(c => c.Name).SingleOrDefaultAsync();
            Assert.AreEqual(res3, "Atlanta");

            Assert.Null(await _db.Cities.SingleOrDefaultAsync(c => c.Population < 100));

            Assert.Null(await _db.Cities.SingleOrDefaultAsync(c => c.Population < 1000000));
        }

        [Test]
        public async Task TestLast()
        {
            var res = await _db.Cities.LastAsync(c => c.Population < 1000000);
            Assert.AreEqual(res.Name, "Atlanta");

            var res2 = await _db.Cities.Where(c => c.Population < 1000000).LastAsync();
            Assert.AreEqual(res2.Name, "Atlanta");

            var res3 = await _db.Cities.Where(c => c.Population < 1000000).Select(c => c.Name).LastAsync();
            Assert.AreEqual(res3, "Boston");

            Assert.ThrowsAsync<InvalidOperationException>(() => _db.Cities.LastAsync(c => c.Population < 100));
        }

        [Test]
        public async Task TestLastOrDefault()
        {
            var res = await _db.Cities.LastOrDefaultAsync(c => c.Population < 1000000);
            Assert.AreEqual(res.Name, "Atlanta");

            var res2 = await _db.Cities.Where(c => c.Population < 1000000).LastOrDefaultAsync();
            Assert.AreEqual(res2.Name, "Atlanta");

            var res3 = await _db.Cities.Where(c => c.Population < 1000000).Select(c => c.Name).LastOrDefaultAsync();
            Assert.AreEqual(res3, "Boston");

            Assert.Null(await _db.Cities.LastOrDefaultAsync(c => c.Population < 100));
        }

        [Test]
        public async Task TestOrderBy()
        {
            var res = await _db.Cities.OrderBy(c => c.Population).ToListAsync();
            Assert.AreEqual(res[0].Name, "Atlanta");

            var res2 = await _db.Cities.OrderByDescending(c => c.Population).ToListAsync();
            Assert.AreEqual(res2[0].Name, "Philadelphia");
        }
    }
}
