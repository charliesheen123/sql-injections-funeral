using NUnit.Framework;
using SQLInjectionsFuneral;
using SQLInjectionsFuneral.Migrator;
using System.Configuration;
using System.Threading.Tasks;

namespace SQLInjectionsFuneral.Tests
{
    /// <summary>
    /// Test for create tables
    /// </summary>
    public class MigratorTests
    {

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestMigrator()
        {
            var cmd = "create";

            Assert.DoesNotThrowAsync(() => Program.Main(new[] { cmd, Configuration.DLL_PATH, Configuration.CONN_STR}));
        }
    }
}