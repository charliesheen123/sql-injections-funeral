﻿using NUnit.Framework;
using SampleModels;
using SQLInjectionsFuneral.Migrator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLInjectionsFuneral.Tests
{
    /// <summary>
    /// Insert, update, delete tests for models without relationships
    /// </summary>
    public class SimpleDMLTests
    {
        private SampleContext _db;

        [SetUp]
        public async Task Setup()
        {
            _db = new SampleContext(Configuration.CONN_STR);

            await Program.Main(new[] { "create", Configuration.DLL_PATH, Configuration.CONN_STR });
        }

        [Test]
        public async Task TestSimpleInsert()
        {
            var philly = new City
            {
                Name = "Philadelphia",
                State = "Pennsylvania (PA)",
                Country = "USA",
                Population = 1600000
            };
            _db.Cities.Add(ref philly);

            var i = await _db.SaveChangesAsync();
            Assert.AreEqual(1, i);
        }

        [Test]
        public async Task TestSimpleUpdate()
        {
            var boston = new City
            {
                Name = "Boston",
                State = "Massachusetts",
                Country = "USA",
                Population = 700000
            };
            _db.Cities.Add(ref boston);
            await _db.SaveChangesAsync();

            boston.State += " (MA)";
            var i = await _db.SaveChangesAsync();
            Assert.AreEqual(1, i);
        }

        [Test]
        public async Task TestSimpleDelete()
        {
            var atlanta = new City
            {
                Name = "Atlanta",
                State = "Georgia (GA)",
                Country = "USA",
                Population = 500000
            };
            _db.Cities.Add(ref atlanta);
            await _db.SaveChangesAsync();
            
            _db.Cities.Remove(atlanta);
            var i = await _db.SaveChangesAsync();
            Assert.AreEqual(1, i);
        }


        #region Transactions
        [Test]
        public async Task TestTransaction()
        {
            var db2 = new SampleContext(Configuration.CONN_STR);

            using (var tr = _db.BeginTransaction())
            {
                var philly = new City
                {
                    Name = "Philadelphia",
                    State = "Pennsylvania (PA)",
                    Country = "USA",
                    Population = 1600000
                };
                _db.Cities.Add(ref philly);
                await _db.SaveChangesAsync();

                var boston = new City
                {
                    Name = "Boston",
                    State = "Massachusetts (MA)",
                    Country = "USA",
                    Population = 700000
                };
                _db.Cities.Add(ref boston);
                await _db.SaveChangesAsync();

                tr.Commit();
            }
            Assert.AreEqual(2, db2.Cities.Count());
        }
        #endregion
    }
}