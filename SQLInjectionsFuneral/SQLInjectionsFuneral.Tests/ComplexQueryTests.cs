﻿using NuGet.Frameworks;
using NUnit.Framework;
using SampleModels;
using SQLInjectionsFuneral.Extensions;
using SQLInjectionsFuneral.Migrator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace SQLInjectionsFuneral.Tests
{
    /// <summary>
    /// Query tests on models with multiple relationships
    /// </summary>
    public class ComplexQueryTests
    {
        private SampleContext _db;

        [SetUp]
        public async Task Setup()
        {
            _db = new SampleContext(Configuration.CONN_STR);

            await Program.Main(new[] { "create", Configuration.DLL_PATH, Configuration.CONN_STR });

            var scranton = new City
            {
                Name = "Scranton",
                State = "Pennsylvania (PA)",
                Country = "USA",
                Population = 77000
            };
            _db.Cities.Add(ref scranton);

            var potus = new Pennsylvanian
            {
                FirstName = "Joe",
                LastName = "Biden",
                BirthDate = new DateTime(1973, 12, 8),
                City = scranton,
                HomeState = "PA",
                ImmigrationDate = null,
                KickedTrumpOut = true
            };
            _db.Pennsylvanians.Add(ref potus);
            await _db.SaveChangesAsync();
        }

        [Test]
        public async Task TestInheritanceLoading()
        {
            var joe = await _db.Pennsylvanians.FirstAsync(a => a.FirstName == "Joe");

            Assert.AreEqual("Biden", joe.LastName);
            Assert.AreEqual("PA", joe.HomeState);
            Assert.IsTrue(joe.KickedTrumpOut);
        }

        [Test]
        public async Task TestSingleNavPropLazyLoading()
        {
            var joe = await _db.Pennsylvanians.FirstAsync(a => a.FirstName == "Joe");

            Assert.AreEqual("Scranton", joe.City.Name);
        }

        [Test]
        public async Task TestSingleNavPropWhere()
        {
            var penns = await _db.Pennsylvanians.Where(p => p.City.Name == "Scranton").ToListAsync();

            Assert.AreEqual(1, penns.Count);
            Assert.AreEqual("Joe", penns[0].FirstName);
        }

        [Test]
        public async Task TestSingleNavPropSelectProp()
        {
            var cityNames = await _db.Pennsylvanians.Select(p => p.City.Name).ToListAsync();

            Assert.AreEqual(1, cityNames.Count);
            Assert.AreEqual("Scranton", cityNames[0]);
        }

        [Test]
        public async Task TestSingleNavPropSelectFull()
        {
            var cities = await _db.Pennsylvanians.Select(p => p.City).ToListAsync();

            Assert.AreEqual(1, cities.Count);
            Assert.AreEqual("Scranton", cities[0].Name);
        }

        [Test]
        public async Task TestSingleNavPropSelectNew()
        {
            var cities = await _db.Pennsylvanians.Select(p => new { p.City.Name, p.City.State, p.FirstName }).ToListAsync();

            Assert.AreEqual(1, cities.Count);
            Assert.AreEqual("Scranton", cities[0].Name);
        }

        [Test]
        public async Task TestSingleNavPropComplex()
        {
            var cityNames = await _db.Pennsylvanians
                .Where(p => p.City.Population < 100000)
                .OrderBy(p => p.City.Name)
                .Select(p => p.City.Name)
                .ToListAsync();

            Assert.AreEqual(1, cityNames.Count);
            Assert.AreEqual("Scranton", cityNames[0]);
        }


        [Test]
        public async Task TestDistinct()
        {
            var joeCocker = new Person
            {
                FirstName = "Joe",
                LastName = "Cocker",
                BirthDate = new DateTime(1944, 5, 20)
            };
            _db.People.Add(ref joeCocker);
            await _db.SaveChangesAsync();

            var res = await _db.People
                .Where(p => p.FirstName == "Joe")
                .Select(p => p.FirstName)
                .Distinct()
                .ToListAsync();

            Assert.AreEqual(1, res.Count);
        }

        [Test]
        public async Task TestInverseNavProp()
        {
            var people = await _db.Cities.Where(c => c.Name == "Scranton")
                .SelectMany(c => c.People)
                .ToListAsync();

            Assert.AreEqual(1, people.Count);
        }

        [Test]
        public async Task TestInverseLazyLoading()
        {
            var scranton = await _db.Cities.FirstAsync(c => c.Name == "Scranton");

            Assert.AreEqual(1, scranton.People.Count);
        }

        [Test]
        public async Task TestMtoNLazyLoading()
        {
            await InsertMtoNTestData();

            var slipknot = await _db.Companies.FirstAsync(c => c.Name == "Slipknot");
            Assert.AreEqual(2, slipknot.People.Count);

            var members = await _db.Companies.Where(c => c.Name == "Slipknot").SelectMany(c => c.People).ToListAsync();
            Assert.AreEqual(2, members.Count);

            Assert.AreEqual(slipknot.Name, members[0].Companies.First().Name);
        }

        private async Task InsertMtoNTestData()
        {
            var company = new Company
            {
                Name = "Slipknot",
                BusinessField = "Heavy Metal",
                LegalForm = "Inc."
            };
            _db.Companies.Add(ref company);
            await _db.SaveChangesAsync();

            var corey = new American
            {
                FirstName = "Corey",
                LastName = "Taylor",
                BirthDate = new DateTime(1973, 12, 8),
                HomeState = "IA",
                ImmigrationDate = null
            };
            _db.Americans.Add(ref corey);

            var joey = new American
            {
                FirstName = "Joey",
                LastName = "Jordison",
                BirthDate = new DateTime(1975, 4, 26),
                HomeState = "IA",
                ImmigrationDate = null
            };
            _db.Americans.Add(ref joey);
            await _db.SaveChangesAsync();

            company.People.Add(corey);
            company.People.Add(joey);
            await _db.SaveChangesAsync();
        }
    }
}
