﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PerformanceTest
{
    public static class Configuration
    {
        public const string CONN_STR = @"Server=localhost\SQLEXPRESS;Initial Catalog=testORM;Persist Security Info=True;Integrated Security=SSPI;MultipleActiveResultSets=True";
        public const string DLL_PATH = @"..\SampleModels\bin\Debug\netcoreapp3.1\SampleModels.dll";
    }
}
