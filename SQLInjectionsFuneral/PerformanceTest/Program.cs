﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using SampleModels;
using SQLInjectionsFuneral.Extensions;

using static PerformanceTest.Configuration;

namespace PerformanceTest.cs
{
    class Program
    {
        const int CITY_COUNT = 100;
        const int PERSON_COUNT = 10000;

        static readonly Random _rnd = new Random();

        static async Task Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.Error.Write("Either enter \"dotnet run create\", \"dotnet run query\" or \"dotnet run query-verbose\".");
                return;
            }

            if (args[0].ToLower() == "create")
            {
                await CreateDb();
                using var db = new SampleContext(CONN_STR);

                Console.WriteLine($"Preparing to insert {CITY_COUNT} cities and {PERSON_COUNT} people...");
                var start = DateTime.Now;
                await CreateCities(db);
                await CreatePeople(db);
                var end = DateTime.Now;

                Console.WriteLine($"{CITY_COUNT} cities and {PERSON_COUNT} people entities created in {(end - start).TotalSeconds} seconds.");
            }
            else if (args[0].ToLower() == "query")
            {
                using var db = new SampleContext(CONN_STR);

                Console.WriteLine($"Preparing to load {CITY_COUNT} cities and {PERSON_COUNT} people...");
                var start = DateTime.Now;
                var (loadedPplCount, loadedCityCount) = await RunQuery(db);
                var end = DateTime.Now;

                Console.WriteLine($"{loadedPplCount} people and {loadedCityCount} loaded in {(end - start).TotalSeconds} seconds.");
            }
            else if (args[0].ToLower() == "query-verbose")
            {
                using var db = new SampleContext(CONN_STR);

                Console.WriteLine($"Preparing to load {CITY_COUNT} cities and {PERSON_COUNT} people and display each record...");
                var start = DateTime.Now;
                RunQueryVerbose(db);
                var end = DateTime.Now;

                Console.WriteLine($"\r\nLoaded in {(end - start).TotalSeconds} seconds.");
            }
            else
            {
                Console.Error.Write("Either enter \"dotnet run create\", \"dotnet run query\" or \"dotnet run query-verbose\".");
            }
        }

        static async Task CreateDb()
        {
            await SQLInjectionsFuneral.Migrator.Program.Main(new[] { "create", DLL_PATH, CONN_STR });
        }

        static async Task CreateCities(SampleContext db)
        {
            var states = new[] { "B", "NÖ", "OÖ", "S", "K", "T", "V", "W", "ST" };
            for (var i = 0; i < CITY_COUNT; i++)
            {
                var city = new City
                {
                    Name = $"City{i + 1}",
                    Country = "Austria",
                    State = states[_rnd.Next(0, 9)],
                    Population = _rnd.Next(1000, 100001)
                };
                db.Cities.Add(ref city);
            }

            await db.SaveChangesAsync();
        }

        static async Task CreatePeople(SampleContext db)
        {
            for (var i = 0; i < PERSON_COUNT; i++)
            {
                var cityId = _rnd.Next(10, CITY_COUNT + 1) * 10;
                //Console.WriteLine(cityId);
                //var city = await db.Cities.FirstAsync(c => c.CityId == cityId);
                var person = new Person
                {
                    FirstName = $"FirstName{i + 1}",
                    LastName = $"LastName{i + 1}",
                    BirthDate = DateTime.Now,
                    CityId = cityId
                };
                db.People.Add(ref person);
            }

            await db.SaveChangesAsync();
        }

        static async Task<(int, int)> RunQuery(SampleContext db)
        {
            var people = await db.People.ToListAsync();

            return (people.Count, people.Select(p => p.City).Distinct().Count());
        }
        static void RunQueryVerbose(SampleContext db)
        {
            foreach (var p in db.People)
            {
                Console.WriteLine($"{p.FirstName} {p.LastName} lives in {p.City.Name}");
            }
        }
    }
}
