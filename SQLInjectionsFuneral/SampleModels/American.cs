﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SampleModels
{
    public class American : Person
    {
        public virtual string HomeState { get; set; }
        public virtual DateTime? ImmigrationDate { get; set; }
    }
}
