﻿using SQLInjectionsFuneral.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace SampleModels
{
    public class Austrian : Person
    {
        public virtual string Province { get; set; }
        [Unique, NotNull]
        public virtual string SVNr { get; set; }
    }
}
