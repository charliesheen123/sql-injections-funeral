﻿using SQLInjectionsFuneral.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace SampleModels
{
    public class City
    {
        public City()
        {
            People = new HashSet<Person>();
        }

        [Key, Identity(100, 10)]
        public virtual int CityId { get; set; }
        public virtual string Name { get; set; }
        public virtual string State { get; set; }
        public virtual string Country { get; set; }
        public virtual int Population { get; set; }

        public virtual ICollection<Person> People { get; set; }
    }
}
