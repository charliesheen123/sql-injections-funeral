﻿using SQLInjectionsFuneral.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace SampleModels
{
    public class Company
    {
        public Company()
        {
            People = new HashSet<Person>();
        }

        [Key]
        public virtual string Name { get; set; }
        public virtual string BusinessField { get; set; }
        public virtual string LegalForm { get; set; }
        public virtual ICollection<Person> People { get; set; }
    }
}
