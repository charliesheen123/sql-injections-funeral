﻿using SQLInjectionsFuneral;
using System;

namespace SampleModels
{
    public class SampleContext : Context
    {
        public SampleContext(string connStr) : base(connStr)
        {
            
        }

        public Table<Person> People { get; set; }
        public Table<Austrian> Austrians { get; set; }
        public Table<American> Americans { get; set; }
        public Table<Pennsylvanian> Pennsylvanians { get; set; }

        public Table<City> Cities { get; set; }
        public Table<Company> Companies { get; set; }
    }
}
