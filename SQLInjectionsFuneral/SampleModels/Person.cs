﻿using SQLInjectionsFuneral.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace SampleModels
{
    public class Person
    {
        public Person()
        {
            Companies = new HashSet<Company>();
        }

        [Key, Identity]
        public virtual int PersId { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime BirthDate { get; set; }
        public virtual int? CityId { get; set; }
        [ForeignKey(nameof(CityId))]
        public virtual City City { get; set; }

        public virtual ICollection<Company> Companies { get; set; }
    }
}
