﻿using SQLInjectionsFuneral.Attributes;
using SQLInjectionsFuneral.Extensions;
using SQLInjectionsFuneral.Metadata;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

#pragma warning disable CS0618 

namespace SQLInjectionsFuneral.DML
{
    /// <summary>
    /// Parser for Insert/Update/Delete operations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DMLParser<T> where T : class, new()
    {
        /// <summary>
        /// Wrapper for a navigation property
        /// </summary>
        private class NavPropWrapper
        {
            internal PropertyInfo PropInfo { get; set; }
            internal ColumnInfo ColInfo { get; set; }
        }

        private readonly Table<T> _table;
        private readonly Context _ctx;

        public DMLParser(Table<T> table, Context ctx)
        {
            _table = table;
            _ctx = ctx;
        }

        /// <summary>
        /// Insert a new entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<int> Insert(object entity)
        {
            Type bt;
            // insert base type
            if ((bt = typeof(T).BaseType) != typeof(object))
            {
                await BaseInsert(entity, bt);
            }

            var props = _table.Type.GetProperties()
                .Where(pi => Mappings.Contain(pi.PropertyType) && _table.ColumnInfos.Where(c => !c.IsIdentity && !c.IsInherited && !c.IsNavigation).Any(c => c.Name == pi.Name))
                .ToList();

            var navProps = _table.Type.GetProperties()
                .Where(pi => _table.ColumnInfos.Where(c => c.IsNavigation && !c.IsInherited).Any(c => c.Name == pi.Name))
                .Select(pi => new NavPropWrapper { PropInfo = pi, ColInfo = _table.ColumnInfos.First(c => c.Name == pi.Name) });

            // load non-collection navigation properties' primary key values
            LoadForeignKeys(entity, props, navProps.Where(n => !n.ColInfo.IsCollection));

            var cmdStr = $"INSERT INTO {_table.Name} ({string.Join(", ", props.Select(p => p.Name))}) VALUES ({string.Join(", ", props.Select(p => $"@{p.Name}"))})";

            using var cmd = new SqlCommand(cmdStr, _ctx.Sql, _ctx.Transaction);

            foreach (var p in props)
            {
                cmd.Parameters.AddWithValue($"@{p.Name}", p.GetValue(entity) ?? DBNull.Value);
            }

            var count = await cmd.ExecuteNonQueryAsync();

            // load identity prop values
            var ip = _table.GetIdentityProp();
            if (ip != null)
            {
                using var select = new SqlCommand("SELECT @@IDENTITY", _ctx.Sql, _ctx.Transaction);
                var idVal = Convert.ToInt32(await select.ExecuteScalarAsync());
                ip.SetValue(entity, idVal);
            }


            var fkCmds = CreateForeignKeyInserts(entity, navProps.Where(n => n.ColInfo.IsCollection));
            foreach (var fkCmd in fkCmds)
            {
                await fkCmd.ExecuteNonQueryAsync();
                await fkCmd.DisposeAsync();
            }

            _table.SetModelState(entity as T, ModelState.Unchanged);

            return count;
        }

        /// <summary>
        /// Update the given entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<int> Update(object entity)
        {
            var keyProps = _table.GetKeyProps();
            var props = _table.Type.GetProperties()
                .Where(pi => Mappings.Contain(pi.PropertyType) && _table.ColumnInfos.Where(c => !c.IsIdentity && !c.IsInherited && !c.IsNavigation).Any(c => c.Name == pi.Name))
                .Except(keyProps);

            // update base type
            await BaseUpdate(entity, keyProps);

            var navProps = _table.Type.GetProperties()
                .Where(pi => _table.ColumnInfos.Where(c => c.IsNavigation && !c.IsInherited).Any(c => c.Name == pi.Name))
                .Select(pi => new NavPropWrapper { PropInfo = pi, ColInfo = _table.ColumnInfos.First(c => c.Name == pi.Name) });

            // load non-collection navigation properties' primary key values into fk value prop
            LoadForeignKeys(entity, props, navProps.Where(n => !n.ColInfo.IsCollection));


            var cmdStr = $"UPDATE {_table.Name} SET {string.Join(", ", props.Select(p => $"{p.Name} = @{p.Name}"))} " +
                $"WHERE {string.Join(" AND ", keyProps.Select(p => $"{p.Name} = @{p.Name}"))}";

            using var cmd = new SqlCommand(cmdStr, _ctx.Sql, _ctx.Transaction);

            foreach (var p in props)
            {
                cmd.Parameters.AddWithValue($"@{p.Name}", p.GetValue(entity) ?? DBNull.Value);
            }
            foreach (var p in keyProps)
            {
                cmd.Parameters.AddWithValue($"@{p.Name}", p.GetValue(entity));
            }
            var count = await cmd.ExecuteNonQueryAsync();


            // update collections
            var fkCmds = CreateForeignKeyUpdates(entity, keyProps, navProps.Where(n => n.ColInfo.IsCollection));
            foreach (var fkCmd in fkCmds)
            {
                await fkCmd.ExecuteNonQueryAsync();
                await fkCmd.DisposeAsync();
            }


            _table.SetModelState(entity as T, ModelState.Unchanged);

            return count;
        }

        /// <summary>
        /// Delete the given entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<int> Delete(object entity)
        {
            var count = 0;
            var keyProps = _table.GetKeyProps();

            var cmdStr = $"DELETE FROM {_table.Name} WHERE {string.Join(" AND ", keyProps.Select(p => $"{p.Name} = @{p.Name}"))}";

            using var cmd = new SqlCommand(cmdStr, _ctx.Sql, _ctx.Transaction);

            foreach (var p in keyProps)
            {
                cmd.Parameters.AddWithValue($"@{p.Name}", p.GetValue(entity));
            }
            count += await cmd.ExecuteNonQueryAsync();


            // delete base type
            Type bt;
            if ((bt = typeof(T).BaseType) != typeof(object))
            {
                var baseTable = _ctx.Tables.First(t => t.GetType().GenericTypeArguments[0] == bt);
                var baseTableName = baseTable.GetType().GetProperty("Name").GetValue(baseTable);

                var baseCmdStr = $"DELETE FROM {baseTableName} WHERE {string.Join(" AND ", keyProps.Select(p => $"{p.Name} = @{p.Name}"))}";

                using var baseCmd = new SqlCommand(baseCmdStr, _ctx.Sql, _ctx.Transaction);

                foreach (var p in keyProps)
                {
                    baseCmd.Parameters.AddWithValue($"@{p.Name}", p.GetValue(entity));
                }

                await baseCmd.ExecuteNonQueryAsync();
            }

            _table._cache.Remove(entity as T);

            return count;
        }

        /// <summary>
        /// Delete the given entity from the collection navigation property with the given name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="entity"></param>
        public void DeleteFromCollection(string name, object entity)
        {
            foreach (var e in _table._cache)
            {
                var coll = _table.Type.GetProperty(name).GetValue(e);
                coll.GetType().GetMethod("Remove").Invoke(coll, new object[] { entity });

                e.GetType().GetProperty("__ModelState").SetValue(e, ModelState.Unchanged);
            }

            var derivedTables = _ctx.Tables.Where(t => t.GetType().GenericTypeArguments[0].BaseType == _table.Type);
            foreach (var dt in derivedTables)
            {
                var derivedDmlParser = dt.GetType().GetProperty("DMLParser").GetValue(dt);
                derivedDmlParser.GetType().GetMethod("DeleteFromCollection").Invoke(derivedDmlParser, new object[] { name, entity });
            }
        }

        #region Advanced insert logic
        /// <summary>
        /// Insert the given entity in a base table (called from a derived table)
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="bt"></param>
        /// <returns></returns>
        private async Task BaseInsert(object entity, Type bt)
        {
            var baseTable = _ctx.Tables.First(t => t.GetType().GenericTypeArguments[0] == bt);
            var baseEntity = Activator.CreateInstance(bt);

            foreach (var p in bt.GetProperties())
            {
                p.SetValue(baseEntity, typeof(T).GetProperty(p.Name).GetValue(entity));
            }

            var baseProxy = baseTable.GetType().GetMethod("CreateProxy").Invoke(baseTable, new object[] { baseEntity });
            var baseDmlParser = baseTable.GetType().GetProperty("DMLParser").GetValue(baseTable);

            await baseDmlParser.GetType().GetMethod("Insert").InvokeAsync(baseDmlParser, new object[] { baseProxy });

            foreach (var p in _table.Type.GetProperties().Where(pi => _table.ColumnInfos.Where(c => c.IsInherited).Any(c => c.Name == pi.Name)))
            {
                p.SetValue(entity, bt.GetProperty(p.Name).GetValue(baseProxy));
            }
        }

        /// <summary>
        /// Insert the foreign keys in related entities for the given entity and its given navigation properties
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="navProps"></param>
        /// <returns></returns>
        private List<SqlCommand> CreateForeignKeyInserts(object entity, IEnumerable<NavPropWrapper> navProps)
        {
            var keyProps = _table.GetKeyProps();
            var fkCmds = new List<SqlCommand>();

            // insert m-to-n and other collection properties into fk value prop 
            foreach (var np in navProps)
            {
                var collValue = np.PropInfo.GetValue(entity) as IEnumerable;
                if (np.ColInfo.IsMtoN)
                {
                    InsertMtoN(entity, np, fkCmds, collValue, keyProps);
                }
                else
                {
                    UpdateForeignKeys(entity, np, fkCmds, collValue, keyProps);
                }
            }

            return fkCmds;
        }
        #endregion


        #region Advanced update logic
        /// <summary>
        /// Update the given entity in a base table (called from a derived table)
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="keyProps"></param>
        /// <returns></returns>
        private async Task BaseUpdate(object entity, IEnumerable<PropertyInfo> keyProps)
        {
            Type bt;
            if ((bt = typeof(T).BaseType) != typeof(object))
            {
                var inheritedProps = _table.Type.GetProperties()
                    .Where(pi => Mappings.Contain(pi.PropertyType) && _table.ColumnInfos.Where(c => c.IsInherited).Any(c => c.Name == pi.Name))
                    .Except(keyProps);

                var baseTable = _ctx.Tables.First(t => t.GetType().GenericTypeArguments[0] == bt);
                var baseTableName = baseTable.GetType().GetProperty("Name").GetValue(baseTable);

                var inheritedNavProps = _table.Type.GetProperties()
                .Where(pi => _table.ColumnInfos.Where(c => c.IsNavigation && c.IsInherited).Any(c => c.Name == pi.Name))
                .Select(pi => new NavPropWrapper { PropInfo = pi, ColInfo = _table.ColumnInfos.First(c => c.Name == pi.Name) });

                // load inherited non-collection navigation properties' primary key values into fk value prop
                LoadForeignKeys(entity, inheritedProps, inheritedNavProps.Where(n => !n.ColInfo.IsCollection));

                var baseCmdStr = $"UPDATE {baseTableName} SET {string.Join(", ", inheritedProps.Select(p => $"{p.Name} = @{p.Name}"))} " +
                $"WHERE {string.Join(" AND ", keyProps.Select(p => $"{p.Name} = @{p.Name}"))}";

                using var baseCmd = new SqlCommand(baseCmdStr, _ctx.Sql, _ctx.Transaction);
                foreach (var p in inheritedProps)
                {
                    baseCmd.Parameters.AddWithValue($"@{p.Name}", p.GetValue(entity) ?? DBNull.Value);
                }

                foreach (var p in keyProps)
                {
                    baseCmd.Parameters.AddWithValue($"@{p.Name}", p.GetValue(entity));
                }

                await baseCmd.ExecuteNonQueryAsync();
            }
        }

        /// <summary>
        /// Update the foreign keys in related entities for the given entity and its given navigation properties with the given key properties
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="keyProps"></param>
        /// <returns></returns>
        private List<SqlCommand> CreateForeignKeyUpdates(object entity, IEnumerable<PropertyInfo> keyProps, IEnumerable<NavPropWrapper> navProps)
        {
            var fkCmds = new List<SqlCommand>();
            foreach (var np in navProps.Where(n => n.ColInfo.IsCollection))
            {
                var collValue = np.PropInfo.GetValue(entity) as IEnumerable;
                if (np.ColInfo.IsMtoN)
                {
                    var delCmdStr = $"DELETE FROM {np.ColInfo.ReferencedTableName} WHERE {_table.Name}_{keyProps.First().Name}=@{_table.Name}_{keyProps.First().Name}";
                    var delCmd = new SqlCommand(delCmdStr, _ctx.Sql, _ctx.Transaction);
                    delCmd.Parameters.AddWithValue($"@{_table.Name}_{keyProps.First().Name}", keyProps.First().GetValue(entity));
                    fkCmds.Add(delCmd);


                    var otherTable = _ctx.Tables.First(t => t.GetType().GenericTypeArguments[0] == collValue.GetType().GenericTypeArguments[0]);
                    var otherDmlParser = otherTable.GetType().GetProperty("DMLParser").GetValue(otherTable);
                    otherDmlParser.GetType().GetMethod("DeleteFromCollection").Invoke(otherDmlParser, new object[] { _table.Name, entity });

                    InsertMtoN(entity, np, fkCmds, collValue, keyProps);
                }
                else
                {
                    UpdateForeignKeys(entity, np, fkCmds, collValue, keyProps);
                }
            }

            return fkCmds;
        }
        #endregion


        #region Misc logic
        /// <summary>
        /// Load all foreign keys of the given entity and its properties from the given navigation properties
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="props"></param>
        /// <param name="navProps"></param>
        private void LoadForeignKeys(object entity, IEnumerable<PropertyInfo> props, IEnumerable<NavPropWrapper> navProps)
        {
            foreach (var np in navProps)
            {
                var npVal = np.PropInfo.GetValue(entity);
                if (npVal == null) continue;

                var keyProp = npVal.GetType().GetProperties().FirstOrDefault(pi => pi.CustomAttributes.Any(a => a.AttributeType == typeof(KeyAttribute)));
                var keyPropVal = keyProp.GetValue(npVal);

                var fkProp = props.First(p => p.Name == np.ColInfo.ForeignKeyPropName);
                fkProp.SetValue(entity, keyPropVal);
            }
        }

        /// <summary>
        /// Create the SQL Commands to update the foreign key relationships
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="np"></param>
        /// <param name="fkCmds"></param>
        /// <param name="coll"></param>
        /// <param name="keyProps"></param>
        private void UpdateForeignKeys(object entity, NavPropWrapper np, List<SqlCommand> fkCmds, IEnumerable coll, IEnumerable<PropertyInfo> keyProps)
        {
            foreach (var collEntry in coll)
            {
                var keyProp = collEntry.GetType().GetProperties().FirstOrDefault(pi => pi.CustomAttributes.Any(a => a.AttributeType == typeof(KeyAttribute)));
                var keyPropVal = keyProp.GetValue(collEntry);

                var navProp = collEntry.GetType().GetProperties().FirstOrDefault(pi => pi.PropertyType == typeof(T));
                navProp.SetValue(collEntry, entity);

                var attrType = navProp.CustomAttributes.First(a => a.AttributeType == typeof(ForeignKeyAttribute)).AttributeType;
                var attr = navProp.GetCustomAttribute(attrType) as ForeignKeyAttribute;

                collEntry.GetType().GetProperty(attr.ForeignKeyName).SetValue(collEntry, keyProps.First().GetValue(entity));

                var updateStr = $"UPDATE {np.ColInfo.ReferencedTableName} SET {attr.ForeignKeyName}=@{attr.ForeignKeyName} WHERE {keyProp.Name}=@{keyProp.Name}";

                var updateRefCmd = new SqlCommand(updateStr, _ctx.Sql, _ctx.Transaction);
                updateRefCmd.Parameters.AddWithValue($"@{keyProp.Name}", keyPropVal);
                updateRefCmd.Parameters.AddWithValue($"@{attr.ForeignKeyName}", keyProps.First().GetValue(entity));

                fkCmds.Add(updateRefCmd);
            }
        }

        /// <summary>
        /// Create the sql commands to insert into m-to-n relationships
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="np"></param>
        /// <param name="fkCmds"></param>
        /// <param name="coll"></param>
        /// <param name="keyProps"></param>
        private void InsertMtoN(object entity, NavPropWrapper np, List<SqlCommand> fkCmds, IEnumerable coll, IEnumerable<PropertyInfo> keyProps)
        {
            foreach (var collEntry in coll)
            {
                var keyProp = collEntry.GetType().GetProperties().FirstOrDefault(pi => pi.CustomAttributes.Any(a => a.AttributeType == typeof(KeyAttribute)));
                var keyPropVal = keyProp.GetValue(collEntry);

                var refPks = new[]
                {
                    new { Name = $"{_table.Name}_{keyProps.First().Name}", Value = keyProps.First().GetValue(entity) },
                    new { Name = $"{np.ColInfo.ReferencedTableName}_{keyProp.Name}", Value = keyPropVal }
                }.OrderBy(n => n.Name).ToList();

                var refTableInsertStr = $"INSERT INTO {np.ColInfo.ReferencedTableName} VALUES (@{refPks[0].Name}, @{refPks[1].Name})";
                var refTableCmd = new SqlCommand(refTableInsertStr, _ctx.Sql, _ctx.Transaction);

                var otherColl = collEntry.GetType().GetProperty(_table.Name).GetValue(collEntry);
                otherColl.GetType().GetMethod("Add").Invoke(otherColl, new object[] { entity });

                collEntry.GetType().GetProperty("__ModelState").SetValue(collEntry, ModelState.Unchanged);

                foreach (var r in refPks)
                {
                    refTableCmd.Parameters.AddWithValue($"@{r.Name}", r.Value);
                }

                fkCmds.Add(refTableCmd);
            }
        }
        #endregion
    }
}
