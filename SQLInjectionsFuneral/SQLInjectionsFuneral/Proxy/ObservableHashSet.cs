﻿using SQLInjectionsFuneral.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SQLInjectionsFuneral.Proxy
{
    /// <summary>
    /// HashSet-like type used in dynamic proxy to track changes applied on collection navigation properties
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ObservableHashSet<T> : ICollection<T>
    {
        private readonly HashSet<T> _internal;
        private bool _hasChanged;
        private readonly PropertyInfo _keyProp;

        /// <summary>
        /// Raised after content has been changed
        /// </summary>
        public event Action Changed;

        /// <summary>
        /// True if content has been changed
        /// </summary>
        public bool HasChanged
        {
            get => _hasChanged;
            set
            {
                if (value)
                {
                    Changed?.Invoke();
                }
                _hasChanged = value;
            }
        }

        public int Count => _internal.Count;

        public bool IsReadOnly => false;

        /// <summary>
        /// Create from another collection
        /// </summary>
        /// <param name="collection"></param>
        public ObservableHashSet(IEnumerable<T> collection)
        {
            _internal = new HashSet<T>(collection);
            _keyProp = typeof(T).GetProperties().First(p => p.CustomAttributes.Any(a => a.AttributeType == typeof(KeyAttribute)));
            //HasChanged = true;
        }

        /// <summary>
        /// Add an item if one with same key does not exist already
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Add(T item)
        {
            if (!_internal.Any(i => _keyProp.GetValue(i).Equals(_keyProp.GetValue(item))))
            {
                HasChanged = true;
                return _internal.Add(item);
            }
            return false;
        }

        /// <summary>
        /// Remove an item if one with same key does exist
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(T item)
        {
            var removed = _internal.RemoveWhere(i => _keyProp.GetValue(i).Equals(_keyProp.GetValue(item))) != 0;
            HasChanged = removed;
            return removed;
        }

        public int RemoveWhere(Predicate<T> match)
        {
            var removed = _internal.RemoveWhere(match);
            return removed;
        }

        void ICollection<T>.Add(T item)
        {
            Add(item);
        }

        public void Clear()
        {
            _internal.Clear();
            HasChanged = true;
        }

        public bool Contains(T item)
        {
            return _internal.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _internal.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _internal.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _internal.GetEnumerator();
        }
    }
}
