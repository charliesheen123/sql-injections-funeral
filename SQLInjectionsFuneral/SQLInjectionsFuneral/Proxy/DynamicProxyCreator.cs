﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Scripting;
using Microsoft.CodeAnalysis.Text;
using SQLInjectionsFuneral.Attributes;
using SQLInjectionsFuneral.Metadata;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.InteropServices;
using System.Text;


namespace SQLInjectionsFuneral.Proxy
{
    /// <summary>
    /// Class for dynamically creating a derived proxy type from the given entity type with the given (additional) properties
    /// </summary>
    internal static class DynamicProxyCreator
    {
        /// <summary>
        /// Create the proxy
        /// </summary>
        /// <param name="typeSignature">Proxy type name</param>
        /// <param name="baseType">Type to create proxy for</param>
        /// <param name="modelStateProp">Property containing model state</param>
        /// <param name="lastStateChange">Property containing last model state change</param>
        /// <param name="overrideProps">Properties to override</param>
        /// <returns></returns>
        internal static Type CreateFrom(string typeSignature, Type baseType, PropInfo modelStateProp, PropInfo lastStateChange, IEnumerable<PropInfo> overrideProps)
        {
            var propCode = "";

            var collStateResetMethod = "";

            var keyProp = overrideProps.First(p => p.CustomAttributes.Any(a => a.AttributeType == typeof(KeyAttribute)));
            foreach (var prop in overrideProps)
            {
                //var isColl = prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(ICollection<>);

                var propTypeName = prop.PropertyType.FullName;

                if (prop.PropertyType.IsGenericType)
                {
                    var collTypeName = prop.PropertyType.Name.Split('`')[0];
                    var genericParams = string.Join(",", prop.PropertyType.GenericTypeArguments.Select(t => t.FullName));
                    propTypeName = $"{prop.PropertyType.Namespace}.{collTypeName}<{genericParams}>";
                }

                var propAttributes = "";

                foreach (var a in prop.CustomAttributes)
                {
                    var @params = string.Join(", ", a.ConstructorArguments.Select(arg => arg.ArgumentType == typeof(string) ? $"\"{arg.Value}\"" : $"{arg.Value}"));
                    propAttributes += $"\r\n    [{a.AttributeType.Name.Replace("Attribute", "")}({@params})]";
                }

                var navPropCode = "";

                if (prop.IsNavigation)
                {
                    if (prop.IsCollection)
                    {
                        navPropCode +=
$@"
            if (_{prop.Name} != null && !(_{prop.Name} is ObservableHashSet<{prop.PropertyType.GenericTypeArguments[0].FullName}>))
            {{
                var set = new ObservableHashSet<{prop.PropertyType.GenericTypeArguments[0].FullName}>(_{prop.Name});
                set.Changed += () => {modelStateProp.Name} = ModelState.Modified;
                _{prop.Name} = set;
            }}

            if (_{prop.Name} == null || {modelStateProp.Name} == ModelState.Unchanged)
            {{
                var set = new ObservableHashSet<{prop.PropertyType.GenericTypeArguments[0].FullName}>(__Table.Where(e => e.{keyProp.Name} == {keyProp.Name}).SelectMany(e => e.{prop.Name}).Distinct().ToHashSet());
                set.Changed += () => {modelStateProp.Name} = ModelState.Modified;
                _{prop.Name} = set;
            }}
";
                        collStateResetMethod +=
$@"
                if(_{prop.Name} is ObservableHashSet<{prop.PropertyType.GenericTypeArguments[0].FullName}> set)
                    set.HasChanged = false;
";
                    }
                    else
                    { 

                        navPropCode +=
$@"
            if (_{prop.Name} == null && {prop.FkName} != null)
            {{
                var fkPropType = GetType().GetProperty(""{prop.FkName}"").PropertyType;                
                _{prop.Name} = __Table.Where(e => e.{keyProp.Name} == {keyProp.Name}).Select(e => e.{prop.Name}).FirstOrDefault();
            }}
";                  
                    }
                }

                propCode +=
$@"
    private {propTypeName} _{prop.Name};
    {(prop.IsCollection ? $"private bool _{prop.Name}HasChanged => (_{prop.Name} as ObservableHashSet<{prop.PropertyType.GenericTypeArguments[0].FullName}>).HasChanged;" : "")}

    {propAttributes}
    public override {propTypeName} {prop.Name} 
    {{ 
        get 
        {{
            {navPropCode}
            return _{prop.Name};
        }}
        set
        {{
            _{prop.Name} = value;
            {modelStateProp.Name} = ModelState.Modified;
        }}
    }}

";
            }

            string code =
$@"
using System;
using System.Collections.Generic;
using System.Linq;
using SQLInjectionsFuneral;
using SQLInjectionsFuneral.Attributes;
using SQLInjectionsFuneral.Metadata;
using SQLInjectionsFuneral.Proxy;


public class {typeSignature} : {baseType.FullName}
{{
    private {modelStateProp.PropertyType.FullName} _{modelStateProp.Name};
    
    public virtual {lastStateChange.PropertyType.FullName} {lastStateChange.Name} {{ get; private set; }}
    public virtual {modelStateProp.PropertyType.FullName} {modelStateProp.Name} 
    {{ 
        get 
        {{
            return _{modelStateProp.Name}; 
        }}
        set 
        {{
            if (value == ModelState.Unchanged)
            {{
                {collStateResetMethod}
            }}
            _{modelStateProp.Name} = value; 
            {lastStateChange.Name} = DateTime.Now; 
        }}
    }}

    public Table<{baseType.FullName}> __Table {{ get; set; }}

    {propCode}
}}

return typeof({typeSignature});
";

            var script = CSharpScript.Create(code, ScriptOptions.Default.WithReferences(Assembly.GetExecutingAssembly(), baseType.Assembly));

            var type = script.RunAsync().Result.ReturnValue as Type;
            return type;
        }

    }
}
