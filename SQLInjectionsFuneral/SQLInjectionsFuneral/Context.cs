﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SQLInjectionsFuneral.Extensions;
using SQLInjectionsFuneral.Metadata;

namespace SQLInjectionsFuneral
{
    /// <summary>
    /// Context class representing the DB connection
    /// </summary>
    public abstract class Context : IDisposable
    {
        /// <summary>
        /// The current transaction (if given)
        /// </summary>
        internal SqlTransaction Transaction { get; private set; }

        /// <summary>
        /// The current SQL Server connection
        /// </summary>
        internal SqlConnection Sql { get; }

        // Connection string to the SQL server
        private readonly string _connStr;

        // For locking purposes
        private static readonly SemaphoreSlim _sem = new SemaphoreSlim(1, 1);

        #region Public API
        public Context(string connectionString)
        {
            _connStr = connectionString;

            Sql = new SqlConnection(_connStr);
            Sql.Open();
            LoadTableInfo();
        }

        /// <summary>
        /// Save all changes (synchronously)
        /// </summary>
        /// <returns></returns>
        public virtual int SaveChanges()
        {
            return SaveChangesAsync().Result;
        }

        /// <summary>
        /// Write all changed entities to DB
        /// </summary>
        /// <returns></returns>
        public virtual async Task<int> SaveChangesAsync()
        {
            try
            {
                await _sem.WaitAsync();
                var count = 0;

                var tables = Tables
                    .Where(t => (bool)t.GetType().GetMethod("HasChanged").Invoke(t, null))
                    .OrderBy(t =>
                    {
                        var dt = (DateTime)t.GetType().GetMethod("GetLastChange").Invoke(t, null);
                        return dt;
                    });

                foreach (var t in tables)
                {
                    var changedEntities = (t.GetType().GetMethod("GetChangedEntities").Invoke(t, null) as IEnumerable<object>)
                        .ToList();

                    var dmlParser = t.GetType().GetProperty("DMLParser").GetValue(t);
                    foreach (var e in changedEntities)
                    {
                        var state = (ModelState)t.GetType().GetMethod("GetModelState").Invoke(t, new object[] { e });

                        if (state == ModelState.Added)
                        {
                            count += (int)await dmlParser.GetType().GetMethod("Insert").InvokeAsync(dmlParser, new object[] { e });
                        }
                        else if (state == ModelState.Modified)
                        {
                            count += (int)await dmlParser.GetType().GetMethod("Update").InvokeAsync(dmlParser, new object[] { e });
                        }
                        else if (state == ModelState.Deleted)
                        {
                            count += (int)await dmlParser.GetType().GetMethod("Delete").InvokeAsync(dmlParser, new object[] { e });
                        }
                    }
                }

                return count;
            }
            finally
            {
                _sem.Release();
            }
        }

        /// <summary>
        /// Begin a new transaction
        /// </summary>
        /// <returns></returns>
        public virtual SqlTransaction BeginTransaction()
        {
            Transaction = Sql.BeginTransaction();
            return Transaction;
        }

        /// <summary>
        /// Begin a new transaction with the given isolation level
        /// </summary>
        /// <param name="isolationLevel"></param>
        /// <returns></returns>
        public virtual SqlTransaction BeginTransaction(System.Data.IsolationLevel isolationLevel)
        {
            Transaction = Sql.BeginTransaction(isolationLevel);
            return Transaction;
        }
        #endregion

        /// <summary>
        /// Load all table infos from the model classes
        /// </summary>
        private void LoadTableInfo()
        {
            var tableInfos = GetType()
                .GetProperties()
                .Where(p => p.PropertyType.GetGenericTypeDefinition() == typeof(Table<>))
                .OrderBy(p => p.Name)
                .Select(p => new { Info = p, Table = Activator.CreateInstance(typeof(Table<>).MakeGenericType(p.PropertyType.GetGenericArguments()[0]), this, p.Name) });


            foreach (var t in tableInfos)
            {
                t.Info.SetValue(this, t.Table);
            }

            foreach (var t in Tables)
            {
                var colInfos = t.GetType().GetProperty("ColumnInfos").GetValue(t) as List<ColumnInfo>;
                foreach (var ci in colInfos)
                {
                    var declaringTable = Tables.First(t => t.GetType().GenericTypeArguments[0] == ci.DeclaringType);
                    ci.DeclaringTableName = declaringTable.GetType().GetProperty("Name").GetValue(declaringTable) as string;
                }
            }
        }

        /// <summary>
        /// All tables
        /// </summary>
        internal IEnumerable<object> Tables =>
            GetType()
            .GetProperties()
            .Where(p => p.PropertyType.GetGenericTypeDefinition() == typeof(Table<>))
            .Select(p => p.GetValue(this));

        /// <summary>
        /// All Proxy types (created once)
        /// </summary>
        internal static Dictionary<Type, Type> ProxyTypes { get; } = new Dictionary<Type, Type>();

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Sql.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Context()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
