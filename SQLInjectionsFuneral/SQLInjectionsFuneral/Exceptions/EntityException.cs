﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLInjectionsFuneral.Exceptions
{
    public class EntityException : ApplicationException
    {
        public int ErrorCode { get; set; }

        public EntityException(string msg) : base(msg)
        {

        }

        public EntityException(string msg, int errorCode) : base(msg)
        {
            ErrorCode = errorCode;
        }
    }
}
