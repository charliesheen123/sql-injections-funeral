﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLInjectionsFuneral
{
    public class NotSupportedException : ApplicationException
    {
        public int ErrorCode { get; set; }

        public NotSupportedException(string msg) : base(msg)
        {

        }

        public NotSupportedException(string msg, int errorCode) : base(msg)
        {
            ErrorCode = errorCode;
        }
    }
}
