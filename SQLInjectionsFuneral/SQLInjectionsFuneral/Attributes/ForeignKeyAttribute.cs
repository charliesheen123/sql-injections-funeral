﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLInjectionsFuneral.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class ForeignKeyAttribute : Attribute
    {
        public ForeignKeyAttribute(string foreignKeyName)
        {
            ForeignKeyName = foreignKeyName;

        }

        public string ForeignKeyName { get; }
    }
}
