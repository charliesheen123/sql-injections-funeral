﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLInjectionsFuneral.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public class UniqueAttribute : Attribute
    {
        public UniqueAttribute()
        {

        }
    }
}
