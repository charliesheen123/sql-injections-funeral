﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace SQLInjectionsFuneral.Extensions
{
    public static class ExpressionExtensions
    {
        public static object Evaluate(this MemberExpression membExpr)
        {
            var objectMember = Expression.Convert(membExpr, typeof(object));
            var getterLambda = Expression.Lambda<Func<object>>(objectMember);
            var getter = getterLambda.Compile();
            return getter();
        }
    }
}
