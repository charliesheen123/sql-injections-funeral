﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SQLInjectionsFuneral.Extensions
{
    public static class AsyncQueryable
    {
        public static async Task<List<T>> ToListAsync<T>(this IQueryable<T> source)
        {
            return await Task.FromResult(source.ToList());
        }

        public static async Task<T[]> ToArrayAsync<T>(this IQueryable<T> source)
        {
            return await Task.FromResult(source.ToArray());
        }

        public static async Task<HashSet<T>> ToHashSetAsync<T>(this IQueryable<T> source)
        {
            return await Task.FromResult(source.ToHashSet());
        }

        public static async Task<Dictionary<TKey, T>> ToDictionaryAsync<TKey, T>(this IQueryable<T> source, Func<T, TKey> keySelector)
        {
            return await Task.FromResult(source.ToDictionary(keySelector));
        }
        public static async Task<Dictionary<TKey, TElement>> ToDictionaryAsync<TKey, TElement>(this IQueryable<TElement> source, Func<TElement, TKey> keySelector, Func<TElement, TElement> elementSelector)
        {
            return await Task.FromResult(source.ToDictionary(keySelector, elementSelector));
        }

        public static async Task<T> FirstAsync<T>(this IQueryable<T> source)
        {
            return await Task.FromResult(source.First());
        }
        public static async Task<T> FirstAsync<T>(this IQueryable<T> source, Expression<Func<T, bool>> predicate)
        {
            return await Task.FromResult(source.First(predicate));
        }
        public static async Task<T> FirstOrDefaultAsync<T>(this IQueryable<T> source)
        {
            return await Task.FromResult(source.FirstOrDefault());
        }
        public static async Task<T> FirstOrDefaultAsync<T>(this IQueryable<T> source, Expression<Func<T, bool>> predicate)
        {
            return await Task.FromResult(source.FirstOrDefault(predicate));
        }

        public static async Task<T> LastAsync<T>(this IQueryable<T> source)
        {
            return await Task.FromResult(source.Last());
        }
        public static async Task<T> LastAsync<T>(this IQueryable<T> source, Expression<Func<T, bool>> predicate)
        {
            return await Task.FromResult(source.Last(predicate));
        }
        public static async Task<T> LastOrDefaultAsync<T>(this IQueryable<T> source)
        {
            return await Task.FromResult(source.LastOrDefault());
        }
        public static async Task<T> LastOrDefaultAsync<T>(this IQueryable<T> source, Expression<Func<T, bool>> predicate)
        {
            return await Task.FromResult(source.LastOrDefault(predicate));
        }

        public static async Task<T> SingleAsync<T>(this IQueryable<T> source)
        {
            return await Task.FromResult(source.Single());
        }
        public static async Task<T> SingleAsync<T>(this IQueryable<T> source, Expression<Func<T, bool>> predicate)
        {
            return await Task.FromResult(source.Single(predicate));
        }
        public static async Task<T> SingleOrDefaultAsync<T>(this IQueryable<T> source)
        {
            return await Task.FromResult(source.SingleOrDefault());
        }
        public static async Task<T> SingleOrDefaultAsync<T>(this IQueryable<T> source, Expression<Func<T, bool>> predicate)
        {
            return await Task.FromResult(source.SingleOrDefault(predicate));
        }

        public static async Task<T> MinAsync<T>(this IQueryable<T> source)
        {
            return await Task.FromResult(source.Min());
        }
        public static async Task<V> MinAsync<T, V>(this IQueryable<T> source, Expression<Func<T, V>> selector) where V : IComparable
        {
            return await Task.FromResult(source.Min(selector));
        }

        public static async Task<T> MaxAsync<T>(this IQueryable<T> source)
        {
            return await Task.FromResult(source.Max());
        }
        public static async Task<V> MaxAsync<T, V>(this IQueryable<T> source, Expression<Func<T, V>> selector) where V : IComparable
        {
            return await Task.FromResult(source.Max(selector));
        }

        public static async Task<int> SumAsync(this IQueryable<int> source)
        {
            return await Task.FromResult(source.Sum());
        }
        public static async Task<int> SumAsync<T>(this IQueryable<T> source, Expression<Func<T, int>> selector)
        {
            return await Task.FromResult(source.Sum(selector));
        }
        public static async Task<double> SumAsync(this IQueryable<double> source)
        {
            return await Task.FromResult(source.Sum());
        }
        public static async Task<double> SumAsync<T>(this IQueryable<T> source, Expression<Func<T, double>> selector)
        {
            return await Task.FromResult(source.Sum(selector));
        }

        public static async Task<double> AverageAsync(this IQueryable<int> source)
        {
            return await Task.FromResult(source.Average());
        }
        public static async Task<double> AverageAsync<T>(this IQueryable<T> source, Expression<Func<T, int>> selector)
        {
            return await Task.FromResult(source.Average(selector));
        }
        public static async Task<double> AverageAsync(this IQueryable<double> source)
        {
            return await Task.FromResult(source.Average());
        }
        public static async Task<double> AverageAsync<T>(this IQueryable<T> source, Expression<Func<T, double>> selector)
        {
            return await Task.FromResult(source.Average(selector));
        }

        public static async Task<int> CountAsync<T>(this IQueryable<T> source)
        {
            return await Task.FromResult(source.Count());
        }
        public static async Task<int> CountAsync<T>(this IQueryable<T> source, Expression<Func<T, bool>> predicate)
        {
            return await Task.FromResult(source.Count(predicate));
        }
    }
}
