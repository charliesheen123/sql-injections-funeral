﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SQLInjectionsFuneral.Query
{
    internal class CacheAccessor
    {
        private readonly HashSet<object> _cache;
        private readonly Type _type;
        internal CacheAccessor(HashSet<object> cache, Type type)
        {
            _cache = cache;
            _type = type;
        }

        internal object Execute(Expression expr)
        {
            if (expr is MethodCallExpression methExpr)
            {
                if (methExpr.Arguments.Count == 2)
                {
                    var method = typeof(Enumerable)
                        .GetMethods()
                        .First(m => m.Name == methExpr.Method.Name && m.GetParameters().Length == 2)
                        .MakeGenericMethod(_type);

                    var lambda = (methExpr.Arguments[1] as UnaryExpression).Operand as LambdaExpression;

                    var typedCache = typeof(Enumerable).GetMethod("Cast").MakeGenericMethod(_type).Invoke(null, new object[] { _cache });

                    return method.Invoke(null, new object[] { typedCache,  lambda.Compile() });
                }
                else
                {
                    var method = typeof(Enumerable)
                        .GetMethods()
                        .First(m => m.Name == methExpr.Method.Name && m.GetParameters().Length == 1)
                        .MakeGenericMethod(_type);

                    var typedCache = typeof(Enumerable).GetMethod("Cast").MakeGenericMethod(_type).Invoke(null, new object[] { _cache });

                    return method.Invoke(null, new object[] { typedCache });
                }
            }
            else
            {
                return null;
            }
        }
    }
}
