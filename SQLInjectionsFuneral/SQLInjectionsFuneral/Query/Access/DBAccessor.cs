﻿using SQLInjectionsFuneral.Attributes;
using SQLInjectionsFuneral.Extensions;
using SQLInjectionsFuneral.Metadata;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SQLInjectionsFuneral.Query
{
    /// <summary>
    /// Class to execute a parsed LINQ query
    /// </summary>
    internal class DBAccessor
    {
        // destination type of query
        private readonly Type _type;

        // source type
        private readonly Type _sourceType;

        // table name
        private readonly string _name;

        // db context
        private readonly Context _ctx;

        // columns of table
        private readonly IEnumerable<ColumnInfo> _columnInfos;

        private readonly HashSet<object> _cache;

        // other types which have been included in query (for joins)
        private readonly HashSet<Type> _includedTypes;

        internal DBAccessor(Type type, Type sourceType, string name, Context ctx, IEnumerable<ColumnInfo> columnInfos, HashSet<object> cache)
        {
            _type = type;
            _sourceType = sourceType;
            _name = name;
            _ctx = ctx;
            _columnInfos = columnInfos;
            _cache = cache;

            _includedTypes = new HashSet<Type>();
        }

        /// <summary>
        /// Execute a parsed query
        /// </summary>
        /// <param name="select"></param>
        /// <param name="where"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        internal async Task<object> ExecuteSqlAsync(QueryBuilder select, QueryBuilder where, QueryBuilder orderBy)
        {
            _includedTypes.Clear();

            var joinStr = CreateJoins(select, where, orderBy);
            var whereStr = where.Empty ? "" : $"WHERE {where}";
            var orderByStr = orderBy.Empty ? "" : $"ORDER BY {orderBy}";
            var selectStr = select.HasColumns ? select.Query : string.Join(", ", _columnInfos.Where(c => !c.IsNavigation).Select(c => c.FullName).OrderBy(n => n));

            var cmdStr = $"SELECT {selectStr} FROM [{_name}] {joinStr} {whereStr} {orderByStr}";
            using var cmd = new SqlCommand(cmdStr, _ctx.Sql, _ctx.Transaction);

            foreach (var param in where.Parameters)
            {
                cmd.Parameters.AddWithValue(param.Key, param.Value);
            }

            var cols = selectStr.Split(", ").ToArray();

            if (select.IsScalar)
            {
                return await cmd.ExecuteScalarAsync();
            }
            else
            {
                return await ExecuteNonScalarAsync(cmd, cols);
            }
        }

        /// <summary>
        /// Create all joins
        /// </summary>
        /// <param name="select"></param>
        /// <param name="where"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        private string CreateJoins(QueryBuilder select, QueryBuilder where, QueryBuilder orderBy)
        {
            var joinStr = "";

            // Base table joins
            var baseType = _sourceType.BaseType;
            var currTableName = _name;

            while (baseType != typeof(object) && baseType != typeof(ValueType))
            {
                var baseTable = _ctx.Tables.First(t => t.GetType().GenericTypeArguments[0] == baseType);
                var baseTableName = baseTable.GetType().GetProperty("Name").GetValue(baseTable) as string;

                var keyProp = (baseTable.GetType().GetMethod("GetKeyProps").Invoke(baseTable, null) as IEnumerable<PropertyInfo>).First();

                joinStr += $" JOIN [{baseTableName}] ON [{currTableName}].[{keyProp.Name}] = [{baseTableName}].[{keyProp.Name}]";

                baseType = baseType.BaseType;
                currTableName = baseTableName;
            }

            // Other joins
            joinStr += CreateSpecificJoins(select);
            joinStr += CreateSpecificJoins(where);
            joinStr += CreateSpecificJoins(orderBy);

            if (_type != _sourceType)
            {
                var type = _type.IsGenericType ? _type.GenericTypeArguments[0] : _type;
                var otherTable = _ctx.Tables.FirstOrDefault(t => t.GetType().GenericTypeArguments[0] == type);
                if (otherTable != null)
                {
                    var otherTableName = otherTable.GetType().GetProperty("Name").GetValue(otherTable) as string;
                    var otherColumnInfos = otherTable.GetType().GetProperty("ColumnInfos").GetValue(otherTable) as List<ColumnInfo>;

                    select.ReplaceTableWithColumns(otherTableName, otherColumnInfos);
                }
            }

            return joinStr;
        }

        /// <summary>
        /// Create joins for specific query part
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private string CreateSpecificJoins(QueryBuilder query)
        {
            var joinStr = "";
            foreach (var kv in query.IncludedTypes)
            {
                var otherTable = _ctx.Tables.First(t => t.GetType().GenericTypeArguments[0] == kv.Key);
                var otherTableName = otherTable.GetType().GetProperty("Name").GetValue(otherTable) as string;
                query.ReplaceTypeParam(kv.Key, otherTableName);

                if (!_includedTypes.Add(kv.Key)) continue;

                var otherKeyName = (otherTable.GetType().GetMethod("GetKeyProps").Invoke(otherTable, null) as IEnumerable<PropertyInfo>).First().Name;
                if (kv.Value.IsCollection)
                {
                    var pkName = _sourceType.GetProperties().First(p => p.CustomAttributes.Any(a => a.AttributeType == typeof(KeyAttribute))).Name;
                    if (kv.Value.IsMtoN)
                    {
                        var joinTableName = string.Join("", new[] { kv.Value.DeclaringTableName, otherTableName }.OrderBy(t => t));
                        joinStr += $" JOIN [{joinTableName}] ON [{kv.Value.DeclaringTableName}].[{pkName}] = [{joinTableName}].[{kv.Value.DeclaringTableName}_{pkName}]" +
                            $" JOIN [{otherTableName}] ON [{joinTableName}].[{otherTableName}_{otherKeyName}] = [{otherTableName}].[{otherKeyName}]";
                    }
                    else
                    {
                        joinStr += $" JOIN [{otherTableName}] ON [{kv.Value.DeclaringTableName}].[{pkName}] = [{otherTableName}].[{pkName}]";
                    }
                }
                else
                {
                    joinStr += $" JOIN [{otherTableName}] ON [{kv.Value.DeclaringTableName}].[{kv.Value.ForeignKeyPropName}] = [{otherTableName}].[{otherKeyName}]";
                }
            }
            return joinStr;
        }

        /// <summary>
        /// Execute the created SQL command expecting a table result
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="cols"></param>
        /// <returns></returns>
        private async Task<object> ExecuteNonScalarAsync(SqlCommand cmd, string[] cols)
        {
            var result = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(_type));
            using var reader = await cmd.ExecuteReaderAsync();
            var derivedType = Context.ProxyTypes.GetValueOrDefault(_type);

            while (await reader.ReadAsync())
            {
                object obj = null;
                if (Mappings.Contain(_type))
                {
                    var value = reader.GetValue(0);
                    value = value is DBNull ? null : value;

                    obj = value;
                }
                else
                {
                    if (_type.GetConstructors().Any(c => c.GetParameters().Length == 0))
                    {
                        if (derivedType != null)
                        {
                            obj = Activator.CreateInstance(derivedType);
                            derivedType.GetProperty("__Table").SetValue(obj, _ctx.Tables.First(t => t.GetType().GenericTypeArguments[0] == _type));
                        }
                        else
                            obj = Activator.CreateInstance(_type);

                        for (var i = 0; i < cols.Length; i++)
                        {
                            var value = reader.GetValue(i);
                            value = value is DBNull ? null : value;

                            _type.GetProperty(ParseToPropName(cols[i])).SetValue(obj, value);
                        }
                    }
                    else
                    {
                        var args = new List<object>();
                        for (var i = 0; i < cols.Length; i++)
                        {
                            var value = reader.GetValue(i);
                            value = value is DBNull ? null : value;

                            args.Add(value);
                        }

                        obj = Activator.CreateInstance(_type, args.ToArray());
                    }
                }

                if (derivedType != null)
                    derivedType.GetProperty("__ModelState").SetValue(obj, ModelState.Unchanged);

                result.Add(obj);

                if (_type == _sourceType)
                    _cache.Add(obj);
            }

            return result;
        }

        /// <summary>
        /// parse a SQL column name to a C# property name
        /// </summary>
        /// <param name="colName"></param>
        /// <returns></returns>
        private string ParseToPropName(string colName)
        {
            var parts = colName.Replace("[", "").Replace("]", "").Split(".");
            return parts[^1];
        }
    }
}
