﻿using SQLInjectionsFuneral.Metadata;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SQLInjectionsFuneral.Query
{
    /// <summary>
    /// Queryable used for table
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class SqlQueryable<T> : IOrderedQueryable<T>
    {
        public Type ElementType => typeof(T);

        public Expression Expression { get; set; }

        public IQueryProvider Provider { get; set; }

        public IEnumerator<T> GetEnumerator()
        {
            return Provider.Execute<IEnumerable<T>>(Expression).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public SqlQueryable(SqlQueryProvider provider)
        {
            Provider = provider;
            Expression = Expression.Constant(this);
        }

        public SqlQueryable(SqlQueryProvider provider, Expression expr)
        {
            Provider = provider;
            Expression = expr;
        }
    }
}
