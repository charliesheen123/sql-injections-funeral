﻿using SQLInjectionsFuneral.Extensions;
using SQLInjectionsFuneral.Metadata;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SQLInjectionsFuneral.Query
{
    /// <summary>
    /// Query provider class
    /// </summary>
    internal class SqlQueryProvider : IQueryProvider
    {
        // Context class
        private readonly Context _ctx;

        // Table name
        private readonly string _name;

        // Destination type of query
        private readonly Type _type;

        // Source type (type of table)
        private readonly Type _sourceType;

        // table columns
        private readonly List<ColumnInfo> _columnInfos;

        private readonly HashSet<object> _cache;

        private readonly DBAccessor _accessor;
        private readonly ExpressionParser _parser;
        private readonly CacheAccessor _cacheAccessor;

        public SqlQueryProvider(Context ctx, string name, Type type, Type sourceType, List<ColumnInfo> columnInfos, HashSet<object> cache)
        {
            _ctx = ctx;
            _name = name;
            _type = type;
            _sourceType = sourceType;
            _columnInfos = columnInfos;
            _cache = cache;

            _accessor = new DBAccessor(_type, _sourceType, _name, _ctx, _columnInfos, _cache);
            _parser = new ExpressionParser(_columnInfos);
            _cacheAccessor = new CacheAccessor(_cache, _type);
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return (IQueryable)Activator.CreateInstance(typeof(SqlQueryable<>).MakeGenericType(_type), new object[] { this, expression });
        }

        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            return new SqlQueryable<TElement>(new SqlQueryProvider(_ctx, _name, typeof(TElement), _sourceType, _columnInfos, _cache), expression);
        }

        public object Execute(Expression expression)
        {
            return Execute<object>(expression);
        }

        /// <summary>
        /// Execute the query
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        public TResult Execute<TResult>(Expression expression)
        {
            if (_type.IsAssignableFrom(typeof(ICollection<>)))
                throw new NotSupportedException($"Type: {_type.FullName} is not supported.");
            //var cacheResult = _cacheAccessor.Execute(expression);

            var select = new QueryBuilder();
            var where = new QueryBuilder();
            var orderBy = new QueryBuilder();

            var (isOrDefault, singleElem) = _parser.Parse<TResult>(expression, select, where, orderBy);

            var result = _accessor.ExecuteSqlAsync(select, where, orderBy).Result;

            if (select.IsScalar && typeof(TResult) == typeof(double))
                result = Convert.ToDouble(result);

            if (typeof(TResult) == _type && result is IList ilist)
            {

                if (singleElem == "first")
                {
                    result = ilist.Count >= 1
                        ? ilist[0]
                        : null;
                }
                else if (singleElem == "single")
                {
                    result = ilist.Count == 1
                        ? ilist[0]
                        : null;
                }
                else if (singleElem == "last")
                {
                    result = ilist.Count >= 1
                        ? ilist[0]
                        : null;
                }

                if (result == null && !isOrDefault)
                    throw new InvalidOperationException(singleElem == "single"
                        ? "The sequence did not contain exactly one element."
                        : "The sequence did not contain any elements.");
            }

            return (TResult)result;
        }
    }
}
