﻿using SQLInjectionsFuneral.Extensions;
using SQLInjectionsFuneral.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Metadata;
using System.Text;

namespace SQLInjectionsFuneral.Query
{
    /// <summary>
    /// Class to parse a LINQ expression representing a query
    /// </summary>
    public class ExpressionParser
    {
        private readonly IEnumerable<ColumnInfo> _columnInfos;

        internal ExpressionParser(IEnumerable<ColumnInfo> columnInfos)
        {
            _columnInfos = columnInfos;
        }

        /// <summary>
        /// Parse the expression, returning if default values (e.g. if "FirstOrDefault" was called) are expected 
        /// and if the result is a single object the called method (e.g. "First" or "Last")
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="expression"></param>
        /// <param name="select"></param>
        /// <param name="where"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        internal (bool, string) Parse<TResult>(Expression expression, QueryBuilder select, QueryBuilder where, QueryBuilder orderBy)
        {
            bool hasPendingParams = false;
            bool isOrDefault = false;
            string singleElem = "";

            while (expression is MethodCallExpression methodExpr)
            {
                if (methodExpr.Method.Name == "Select" || methodExpr.Method.Name == "SelectMany")
                {
                    if (!select.IsScalar || hasPendingParams)
                    {
                        var exprBody = ((methodExpr.Arguments[1] as UnaryExpression).Operand as LambdaExpression).Body;
                        ParseSelect<TResult>(select, exprBody);

                        if (hasPendingParams)
                            select.Append(")");
                    }
                }
                else if (methodExpr.Method.Name == "Where")
                {
                    var exprBody = ((methodExpr.Arguments[1] as UnaryExpression).Operand as LambdaExpression).Body;

                    if (!where.Empty)
                        where.Append(" AND ");

                    ParseWhere<TResult>(where, exprBody);
                }
                else if (methodExpr.Method.Name == "Count")
                {
                    select.Clear();
                    select.IsScalar = true;
                    select.Append("COUNT(*)");

                    if (methodExpr.Arguments.Count > 1)
                    {
                        var exprBody = ((methodExpr.Arguments[1] as UnaryExpression).Operand as LambdaExpression).Body;
                        if (!where.Empty)
                            where.Append(" AND ");

                        ParseWhere<TResult>(where, exprBody);
                    }
                }
                else if (ParamAggregateFuncs.Contain(methodExpr.Method.Name))
                {
                    if (methodExpr.Arguments.Count > 1)
                    {
                        var exprBody = ((methodExpr.Arguments[1] as UnaryExpression).Operand as LambdaExpression).Body;

                        select.Clear();
                        select.IsScalar = true;
                        select.Append($"{ParamAggregateFuncs.AggregateFuncs[methodExpr.Method.Name]}(");

                        ParseSelect<TResult>(select, exprBody);
                        select.Append(")");
                    }
                    else
                    {
                        select.IsScalar = true;
                        select.Prepend($"{ParamAggregateFuncs.AggregateFuncs[methodExpr.Method.Name]}(");
                        hasPendingParams = true;
                    }
                }
                else if (methodExpr.Method.Name == "First")
                {
                    select.Prepend("TOP 1 ");
                    if (methodExpr.Arguments.Count > 1)
                    {
                        var exprBody = ((methodExpr.Arguments[1] as UnaryExpression).Operand as LambdaExpression).Body;
                        if (!where.Empty)
                            where.Append(" AND ");

                        ParseWhere<TResult>(where, exprBody);
                    }

                    singleElem = "first";
                }
                else if (methodExpr.Method.Name == "FirstOrDefault")
                {
                    select.Prepend("TOP 1 ");

                    if (methodExpr.Arguments.Count > 1)
                    {
                        var exprBody = ((methodExpr.Arguments[1] as UnaryExpression).Operand as LambdaExpression).Body;
                        if (!where.Empty)
                            where.Append(" AND ");

                        ParseWhere<TResult>(where, exprBody);
                    }

                    singleElem = "first";
                    isOrDefault = true;
                }
                else if (methodExpr.Method.Name == "Single")
                {
                    if (methodExpr.Arguments.Count > 1)
                    {
                        var exprBody = ((methodExpr.Arguments[1] as UnaryExpression).Operand as LambdaExpression).Body;
                        if (!where.Empty)
                            where.Append(" AND ");

                        ParseWhere<TResult>(where, exprBody);
                    }

                    singleElem = "single";
                }
                else if (methodExpr.Method.Name == "SingleOrDefault")
                {
                    if (methodExpr.Arguments.Count > 1)
                    {
                        var exprBody = ((methodExpr.Arguments[1] as UnaryExpression).Operand as LambdaExpression).Body;
                        if (!where.Empty)
                            where.Append(" AND ");

                        ParseWhere<TResult>(where, exprBody);
                    }

                    singleElem = "single";
                    isOrDefault = true;
                }
                else if (methodExpr.Method.Name == "Last")
                {
                    select.Prepend("TOP 1 ");
                    orderBy.Prepend("1 DESC ");

                    if (methodExpr.Arguments.Count > 1)
                    {
                        var exprBody = ((methodExpr.Arguments[1] as UnaryExpression).Operand as LambdaExpression).Body;
                        if (!where.Empty)
                            where.Append(" AND ");

                        ParseWhere<TResult>(where, exprBody);
                    }

                    singleElem = "last";
                }
                else if (methodExpr.Method.Name == "LastOrDefault")
                {
                    select.Prepend("TOP 1 ");
                    orderBy.Prepend("1 DESC ");

                    if (methodExpr.Arguments.Count > 1)
                    {
                        var exprBody = ((methodExpr.Arguments[1] as UnaryExpression).Operand as LambdaExpression).Body;
                        if (!where.Empty)
                            where.Append(" AND ");

                        ParseWhere<TResult>(where, exprBody);
                    }

                    singleElem = "last";
                    isOrDefault = true;
                }
                else if (methodExpr.Method.Name.StartsWith("OrderBy") || methodExpr.Method.Name.StartsWith("ThenBy"))
                {
                    var exprBody = ((methodExpr.Arguments[1] as UnaryExpression).Operand as LambdaExpression).Body;
                    ParseOrderBy<TResult>(orderBy, exprBody, methodExpr.Method.Name.EndsWith("Descending"));
                }
                else if (methodExpr.Method.Name == "Distinct")
                {
                    select.Prepend("DISTINCT ");
                }
                else
                {
                    throw new NotSupportedException($"The Method {methodExpr.Method.Name} is not supported.");
                }

                expression = methodExpr.Arguments[0];
            }

            return (isOrDefault, singleElem);
        }

        /// <summary>
        /// Parse the select part of the expression
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="expr"></param>
        private void ParseSelect<T>(QueryBuilder query, Expression expr)
        {
            if (expr is MemberExpression membExpr)
            {
                var navPropPath = "";
                while (membExpr.Expression.NodeType != ExpressionType.Parameter)
                {
                    navPropPath += $".[{membExpr.Member.Name}]";
                    membExpr = membExpr.Expression as MemberExpression;
                }

                var colInfo = _columnInfos.FirstOrDefault(c => c.Name == membExpr.Member.Name);
                if (colInfo == null)
                    throw new NotSupportedException("Failed to get property member.");

                var memberType = (membExpr.Member as PropertyInfo).PropertyType;
                memberType = memberType.IsGenericType ? memberType.GenericTypeArguments[0] : memberType;
                navPropPath = $"[:{memberType.Name}]{navPropPath}";

                if (colInfo.IsNavigation)
                {
                    query.IncludedTypes.TryAdd(memberType, colInfo);
                    query.Append($" {navPropPath}");
                }
                else
                    query.Append($" {colInfo.FullName}");
            }
            else if (expr is NewExpression newExpr)
            {
                foreach (var arExpr in newExpr.Arguments)
                {
                    ParseSelect<T>(query, arExpr);

                    if (arExpr != newExpr.Arguments.Last())
                    {
                        query.Append(", ");
                    }
                }
            }
            else
            {
                throw new NotSupportedException($"Only anonymous types or single properties are supported in select clause.");
            }
        }

        /// <summary>
        /// Parse the where part of the expression
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="expr"></param>
        /// <param name="isRight"></param>
        private void ParseWhere<T>(QueryBuilder query, Expression expr, bool isRight = false)
        {
            if (expr is BinaryExpression binExpr)
            {
                query.Append("(");

                ParseWhere<T>(query, binExpr.Left);

                if (!ExprTypes.TryGetValue(binExpr.NodeType, out string val))
                    throw new NotSupportedException($"Expression type {binExpr.NodeType} is not supported.");

                if (binExpr.Right is ConstantExpression rightExpr && rightExpr.Value == null)
                {
                    if (binExpr.NodeType == ExpressionType.Equal)
                        val = "IS";
                    else if (binExpr.NodeType == ExpressionType.NotEqual)
                        val = "IS NOT";
                    else
                        throw new NotSupportedException($"Invalid null comparison with expression type: {binExpr.NodeType}");
                }

                query.Append($" {val}");

                ParseWhere<T>(query, binExpr.Right, isRight: true);
                query.Append(")");
            }
            else if (expr is MemberExpression membExpr)
            {
                if (isRight)
                {
                    var value = membExpr.Evaluate();

                    if (value == null)
                        query.Append("NULL");
                    else
                        query.AddParam(value);
                }
                else
                {
                    var navPropPath = "";
                    while (membExpr.Expression.NodeType == ExpressionType.MemberAccess)
                    {
                        navPropPath += $".[{membExpr.Member.Name}]";
                        membExpr = membExpr.Expression as MemberExpression;
                    }

                    var colInfo = _columnInfos.FirstOrDefault(c => c.Name == membExpr.Member.Name);
                    if (colInfo == null)
                        throw new NotSupportedException("Failed to get property member.");

                    var memberType = (membExpr.Member as PropertyInfo).PropertyType;
                    memberType = memberType.IsGenericType ? memberType.GenericTypeArguments[0] : memberType;
                    navPropPath = $"[:{memberType.Name}]{navPropPath}";

                    if (colInfo.IsNavigation)
                    {
                        query.IncludedTypes.TryAdd(memberType, colInfo);
                        query.Append($" {navPropPath}");
                    }
                    else
                        query.Append($" {colInfo.FullName}");
                }
            }
            else if (expr is UnaryExpression unExpr)
            {
                if (unExpr.IsLiftedToNull)
                    ParseWhere<T>(query, unExpr.Operand);
                else
                    throw new NotSupportedException("Invalid unary expression.");
            }
            else if (expr is ConstantExpression constExpr)
            {
                query.Append(" ");

                if (constExpr.Value == null)
                    query.Append("NULL");
                else
                    query.AddParam(constExpr.Value);
            }
            else
            {
                throw new NotSupportedException($"Expression type {expr.GetType().Name} is not supported.");
            }
        }

        /// <summary>
        /// Parse an order by part of the expression
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="expr"></param>
        /// <param name="isDesc"></param>
        private void ParseOrderBy<T>(QueryBuilder query, Expression expr, bool isDesc)
        {
            if (expr is MemberExpression membExpr)
            {
                var navPropPath = "";
                while (membExpr.Expression.NodeType != ExpressionType.Parameter)
                {
                    navPropPath += $".[{membExpr.Member.Name}]";
                    membExpr = membExpr.Expression as MemberExpression;
                }

                var colInfo = _columnInfos.FirstOrDefault(c => c.Name == membExpr.Member.Name);
                if (colInfo == null)
                    throw new NotSupportedException("Failed to get property member.");

                if (!query.Empty)
                    query.Append(", ");

                var memberType = (membExpr.Member as PropertyInfo).PropertyType;
                memberType = memberType.IsGenericType ? memberType.GenericTypeArguments[0] : memberType;
                navPropPath = $"[:{memberType.Name}]{navPropPath}";

                if (colInfo.IsNavigation)
                {
                    query.IncludedTypes.TryAdd(memberType, colInfo);
                    query.Append($" {navPropPath} {(isDesc ? "DESC" : "ASC")}");
                }
                else
                    query.Append($" {colInfo.FullName} {(isDesc ? "DESC" : "ASC")}");
            }
            else
            {
                throw new NotSupportedException($"Only single properties are supported in order by clause.");
            }
        }

        /// <summary>
        /// ExpressionType to SQL Server symbol mapping
        /// </summary>
        private Dictionary<ExpressionType, string> ExprTypes => new Dictionary<ExpressionType, string>
        {
            { ExpressionType.Equal, "=" },
            { ExpressionType.NotEqual, "<>" },
            { ExpressionType.GreaterThan, ">" },
            { ExpressionType.GreaterThanOrEqual, ">=" },
            { ExpressionType.LessThan, "<" },
            { ExpressionType.LessThanOrEqual, "<=" },
            { ExpressionType.AndAlso, "AND" },
            { ExpressionType.OrElse, "OR" }
        };
    }
}
