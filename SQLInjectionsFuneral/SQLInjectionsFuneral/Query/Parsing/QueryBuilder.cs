﻿using SQLInjectionsFuneral.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQLInjectionsFuneral.Query
{
    /// <summary>
    /// Class to create a SQL query
    /// </summary>
    internal class QueryBuilder
    {
        // query parameters
        private readonly Dictionary<string, object> _params;
        private StringBuilder _builder;

        internal string Query => _builder.ToString();
        internal bool Empty => Query.Length == 0;

        // if columns have been selected (only if select query)
        internal bool HasColumns => Query.Replace("TOP 1 ", "").Length > 0;
        internal bool IsScalar { get; set; }

        // all types which are necessary for joins
        internal Dictionary<Type, ColumnInfo> IncludedTypes { get; }
        internal List<KeyValuePair<string, object>> Parameters => _params.ToList();

        internal QueryBuilder()
        {
            _params = new Dictionary<string, object>();
            _builder = new StringBuilder();
            IncludedTypes = new Dictionary<Type, ColumnInfo>();
        }

        /// <summary>
        /// Append a new query part
        /// </summary>
        /// <param name="str"></param>
        internal void Append(string str)
        {
            _builder.Append(str);
        }

        /// <summary>
        /// Prepend a new query part
        /// </summary>
        /// <param name="str"></param>
        internal void Prepend(string str)
        {
            _builder.Insert(0, str);
        }

        /// <summary>
        /// Add a parameter with the given data
        /// </summary>
        /// <param name="data"></param>
        internal void AddParam(object data)
        {
            var name = $"__param_{_params.Count}";
            _builder.Append($"@{name}");
            _params.Add(name, data);
        }

        /// <summary>
        /// Replace a type placeholder with the actual table name (during join process)
        /// </summary>
        /// <param name="type"></param>
        /// <param name="tableName"></param>
        internal void ReplaceTypeParam(Type type, string tableName)
        {
            _builder = _builder.Replace($":{type.Name}", tableName);
        }

        /// <summary>
        /// Replace a table name with its actual columns (inverse join)
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnInfos"></param>
        internal void ReplaceTableWithColumns(string tableName, List<ColumnInfo> columnInfos)
        {
            _builder = _builder.Replace($"[{tableName}]", string.Join(", ", columnInfos.Where(c => !c.IsNavigation).Select(c => c.FullName).OrderBy(n => n)));
        }

        /// <summary>
        /// clear the whole query string
        /// </summary>
        internal void Clear()
        {
            _builder.Clear();
        }

        public override string ToString()
        {
            return Query;
        }
    }
}
