﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLInjectionsFuneral.Metadata
{
    public enum ModelState
    {
        Unchanged = 0,
        Added = 1,
        Modified = 2,
        Deleted = 3
    }
}
