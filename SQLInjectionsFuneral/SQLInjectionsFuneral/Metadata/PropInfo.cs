﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace SQLInjectionsFuneral.Metadata
{
    /// <summary>
    /// Class containing property information for dynamic proxy creation
    /// </summary>
    internal class PropInfo
    {
        internal string Name { get; set; }
        internal Type PropertyType { get; set; }
        internal Type DeclaringType { get; set; }
        internal IEnumerable<CustomAttributeData> CustomAttributes { get; set; }
        internal bool IsNavigation { get; set; }
        internal string FkName { get; set; }
        internal bool IsCollection { get; set; }
        internal bool IsMtoN { get; set; }
    }
}
