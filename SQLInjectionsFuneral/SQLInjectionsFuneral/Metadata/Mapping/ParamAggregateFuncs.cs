﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLInjectionsFuneral.Metadata
{
    /// <summary>
    /// LINQ aggregate functions mapped to SQL Server aggregate functions
    /// </summary>
    public static class ParamAggregateFuncs
    {
        public static Dictionary<string, string> AggregateFuncs => new Dictionary<string, string>
        {
            { "Sum", "SUM" },
            { "Min", "MIN" },
            { "Max", "MAX" },
            { "Average", "AVG" }
        };

        public static bool Contain(string fn) => AggregateFuncs.ContainsKey(fn);
    }
}
