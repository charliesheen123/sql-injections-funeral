﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQLInjectionsFuneral.Metadata
{
    /// <summary>
    /// C# types mapped to SQL Server types
    /// </summary>
    public static class Mappings
    {
        public static Dictionary<Type, string> TypeMappings => new Dictionary<Type, string>
        {
            { typeof(bool), "BIT" },
            { typeof(byte), "TINYINT" },
            { typeof(short), "SMALLINT" },
            { typeof(int), "INT" },
            { typeof(long), "BIGINT" },
            { typeof(float), "FLOAT" },
            { typeof(double), "FLOAT" },
            { typeof(decimal), "DECIMAL" },
            { typeof(DateTime), "DATETIME" },
            { typeof(bool?), "BIT" },
            { typeof(byte?), "TINYINT" },
            { typeof(short?), "SMALLINT" },
            { typeof(int?), "INT" },
            { typeof(long?), "BIGINT" },
            { typeof(float?), "FLOAT" },
            { typeof(double?), "FLOAT" },
            { typeof(decimal?), "DECIMAL" },
            { typeof(DateTime?), "DATETIME" },
            { typeof(string), "NVARCHAR(MAX)" }
        };

        public static bool Contain(Type t) => TypeMappings.ContainsKey(t);
    }
}
