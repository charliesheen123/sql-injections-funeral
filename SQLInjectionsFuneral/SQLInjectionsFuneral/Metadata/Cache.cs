﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQLInjectionsFuneral.Metadata
{
    internal class Cache
    {
        private Dictionary<object, object> _cache;
        private HashSet<object> _untrackedCache;

        internal Cache()
        {
            _cache = new Dictionary<object, object>();
            _untrackedCache = new HashSet<object>();
        }

        internal void Add(object key, object value)
        {
            _cache.Add(key, value);
        }

        internal void Remove(object key)
        {
            _cache.Remove(key);
        }

        internal void AddUntracked(object value)
        {
            _untrackedCache.Add(value);
        }

        internal void Track(object key, object value)
        {
            if (_untrackedCache.Remove(value))
            {
                _cache.Add(key, value);
            }
        }

        internal IEnumerable<object> TrackedValues => _cache.Values;

        internal IEnumerable<object> AllValues => _untrackedCache.Union(_cache.Values);
    }
}
