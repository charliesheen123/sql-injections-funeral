﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SQLInjectionsFuneral.Attributes;
using SQLInjectionsFuneral.DML;
using SQLInjectionsFuneral.Exceptions;
using SQLInjectionsFuneral.Extensions;
using SQLInjectionsFuneral.Metadata;
using SQLInjectionsFuneral.Proxy;
using SQLInjectionsFuneral.Query;

#pragma warning disable CS0618

namespace SQLInjectionsFuneral
{
    public sealed class Table<T> : IOrderedQueryable<T> where T : class, new()
    {
        /// <summary>
        /// Name of the table (in DB)
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Parser class for DML operations
        /// </summary>
        public DMLParser<T> DMLParser { get; }

        /// <summary>
        /// All column informations
        /// </summary>
        public List<ColumnInfo> ColumnInfos { get; }

        /// <summary>
        /// Contained type
        /// </summary>
        internal Type Type { get; }

        internal readonly HashSet<object> _cache;
        private readonly Context _ctx;
        private readonly Type _derivedType;
        private readonly List<PropInfo> _propInfos;
        private readonly SqlQueryable<T> _queryable;

        private static readonly PropInfo[] _derivedProps;

        static Table()
        {
            _derivedProps = new PropInfo[]
            {
                new PropInfo { Name = "__ModelState", PropertyType = typeof(ModelState) },
                new PropInfo { Name = "__LastStateChange", PropertyType = typeof(DateTime) },
            };
        }

        public Table(Context c, string name)
        {
            Type = typeof(T);
            Name = name;
            _cache = new HashSet<object>();
            ColumnInfos = new List<ColumnInfo>();
            _propInfos = new List<PropInfo>();
            _ctx = c;

            LoadColumnInfos();

            if (Context.ProxyTypes.TryGetValue(Type, out var derived))
                _derivedType = derived;
            else
            {
                _derivedType = DynamicProxyCreator.CreateFrom($"{Type.Name}_DynamicProxy_{Guid.NewGuid().ToString().Replace("-", "")}", Type, _derivedProps[0], _derivedProps[1], _propInfos);
                Context.ProxyTypes.Add(Type, _derivedType);
            }

            _queryable = new SqlQueryable<T>(new SqlQueryProvider(_ctx, Name, typeof(T), typeof(T), ColumnInfos, _cache));
            DMLParser = new DMLParser<T>(this, _ctx);
        }

        #region Public API
        /// <summary>
        /// Add a new entity
        /// </summary>
        /// <param name="entity"></param>
        public void Add(ref T entity)
        {
            entity = CreateProxy(entity) as T;
            SetModelState(entity, ModelState.Added);

            _cache.Add(entity);

        }

        /// <summary>
        /// Remove a entity
        /// </summary>
        /// <param name="entity"></param>
        public void Remove(T entity)
        {
            SetModelState(entity, ModelState.Deleted);
        }


        #region IQueryable
        public Type ElementType => _queryable.ElementType;

        public Expression Expression => _queryable.Expression;

        public IQueryProvider Provider => _queryable.Provider;

        public IEnumerator<T> GetEnumerator()
        {
            return _queryable.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _queryable.GetEnumerator();
        }
        #endregion

        #endregion


        #region Internal Methods
        /// <summary>
        /// Get all entities which have been changed
        /// </summary>
        /// <returns></returns>
        [Obsolete("Only for internal purposes.")]
        public IEnumerable<T> GetChangedEntities()
        {
            foreach (var e in _cache)
            {
                if (GetModelState(e as T) != ModelState.Unchanged)
                    yield return e as T;
            }
        }

        /// <summary>
        /// Get the state of the given entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>

        [Obsolete("Only for internal purposes.")]
        public ModelState GetModelState(T entity)
        {
            return (ModelState)_derivedType.GetProperty("__ModelState").GetValue(entity);
        }

        /// <summary>
        /// True, if at least one entity has changed
        /// </summary>
        /// <returns></returns>

        [Obsolete("Only for internal purposes.")]
        public bool HasChanged()
        {
            var hasChanged = false;
            foreach (var entity in _cache)
            {
                var state = (ModelState)_derivedType.GetProperty("__ModelState").GetValue(entity);
                if (state != ModelState.Unchanged)
                    hasChanged = true;
            }

            return hasChanged;
        }

        /// <summary>
        /// Get time of last change of any entity
        /// </summary>
        /// <returns></returns>

        [Obsolete("Only for internal purposes.")]
        public DateTime GetLastChange()
        {
            var last = default(DateTime);

            foreach (var entity in _cache)
            {
                var change = (DateTime)_derivedType.GetProperty("__LastStateChange").GetValue(entity);
                if (change > last)
                    last = change;
            }

            return last;
        }

        /// <summary>
        /// Create a proxy instance based on the given entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>

        [Obsolete("Only for internal purposes.")]
        public object CreateProxy(T entity)
        {
            var instance = Activator.CreateInstance(_derivedType);

            foreach (var prop in Type.GetProperties())
            {
                if (!IsVirtual(prop))
                {
                    throw new NotSupportedException($"The Property '{prop.Name}' has to be declared as VIRTUAL to be mapped.");
                }
                else if (!IsMappable(prop))
                {
                    throw new NotSupportedException($"The Type '{prop.PropertyType.FullName}' is not mappable.");
                }
                else
                {
                    _derivedType.GetProperty(prop.Name).SetValue(instance, prop.GetValue(entity));
                }
            }

            //_derivedType.GetProperty("__Tables").SetValue(instance, _ctx.Tables);
            _derivedType.GetProperty("__Table").SetValue(instance, this);
            _derivedType.GetProperty("__ModelState").SetValue(instance, ModelState.Unchanged);

            return instance;
        }

        /// <summary>
        /// Get all key properties (in fact always 1)
        /// </summary>
        /// <returns></returns>

        [Obsolete("Only for internal purposes.")]
        public IEnumerable<PropertyInfo> GetKeyProps()
        {
            return Type.GetProperties().Where(pi => pi.CustomAttributes.Any(a => a.AttributeType == typeof(KeyAttribute)));
        }

        /// <summary>
        /// Get an identity property
        /// </summary>
        /// <returns></returns>
        internal PropertyInfo GetIdentityProp()
        {
            return Type.GetProperties().FirstOrDefault(pi => pi.CustomAttributes.Any(a => a.AttributeType == typeof(IdentityAttribute)));
        }

        /// <summary>
        /// Set the model state of the passed entity to the passed state value
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="state"></param>

        internal void SetModelState(T entity, ModelState state)
        {
            _derivedType.GetProperty("__ModelState").SetValue(entity, state);
        }

        /// <summary>
        /// Load all column infos
        /// </summary>
        private void LoadColumnInfos()
        {
            var hasKey = false;
            var props = Type.GetProperties();

            foreach (var pi in props)
            {
                if (!IsMappable(pi)) throw new NotSupportedException($"The Type '{pi.PropertyType.FullName}' is not mappable.");
                if (!IsVirtual(pi)) throw new NotSupportedException($"The Property '{pi.Name}' has to be declared as VIRTUAL to be mapped.");
            }

            var columnProps = props.Where(pi => Mappings.Contain(pi.PropertyType));
            var navigationProps = props.Where(pi => !Mappings.Contain(pi.PropertyType));//IsNavigationProp(pi));

            foreach (var pi in columnProps)
            {
                var hasNotNullAttr = pi.CustomAttributes.Any(a => a.AttributeType == typeof(NotNullAttribute));

                IdentityAttribute identityAttr = null;
                var identityAttrType = pi.CustomAttributes.FirstOrDefault(a => a.AttributeType == typeof(IdentityAttribute))?.AttributeType;
                if (identityAttrType != null)
                    identityAttr = pi.GetCustomAttribute(identityAttrType) as IdentityAttribute;

                var inheritanceLevel = 0;
                var currType = typeof(T);
                while (pi.DeclaringType != currType)
                {
                    inheritanceLevel++;
                    currType = currType.BaseType;
                }

                var colInfo = new ColumnInfo
                {
                    Name = pi.Name,
                    Type = Mappings.TypeMappings[pi.PropertyType],
                    DeclaringType = pi.DeclaringType,
                    ForeignKeyPropName = null,
                    IsNavigation = false,
                    IsNullable = !hasNotNullAttr && ((pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)) || !pi.PropertyType.IsValueType),
                    IsInherited = pi.DeclaringType != typeof(T),
                    InheritanceLevel = inheritanceLevel,
                    IsUnique = pi.CustomAttributes.Any(a => a.AttributeType == typeof(UniqueAttribute)),
                    IsIdentity = identityAttr != null,
                    IdentitySeed = identityAttr?.Seed,
                    IdentityIncrement = identityAttr?.Increment
                };

                if (pi.CustomAttributes.Any(a => a.AttributeType == typeof(KeyAttribute)))
                {
                    hasKey = true;
                    colInfo.IsKey = true;
                }
                else
                {
                    colInfo.IsKey = false;
                }

                ColumnInfos.Add(colInfo);
                _propInfos.Add(new PropInfo
                {
                    Name = pi.Name,
                    PropertyType = pi.PropertyType,
                    CustomAttributes = pi.CustomAttributes,
                    DeclaringType = pi.DeclaringType
                });
            }

            foreach (var pi in navigationProps)
            {
                var isCollection = pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition() == typeof(ICollection<>);

                var collContentType = isCollection ? pi.PropertyType.GenericTypeArguments[0] : null;
                var isMtoN = isCollection && collContentType.GetProperties().Any(p => p.PropertyType.IsGenericType && p.PropertyType.GenericTypeArguments[0] == pi.DeclaringType);

                if (pi.CustomAttributes.Any(a => a.AttributeType == typeof(KeyAttribute)))
                    throw new NotSupportedException($"The Type '{pi.PropertyType.FullName}' is invalid for primary key usage.");

                if (!isCollection && !pi.CustomAttributes.Any(a => a.AttributeType == typeof(ForeignKeyAttribute)))
                    throw new EntityException($"Navigation properties must be decorated with a {typeof(ForeignKeyAttribute).FullName}");

                string fkName = null;
                if (!isCollection)
                {
                    var attrType = pi.CustomAttributes.First(a => a.AttributeType == typeof(ForeignKeyAttribute)).AttributeType;
                    var attr = pi.GetCustomAttribute(attrType) as ForeignKeyAttribute;
                    fkName = attr.ForeignKeyName;
                }

                //var fkCol = ColumnInfos.FirstOrDefault(c => c.Name == fkName) ?? throw new EntityException($"Invalid foreign key name: {fkName}");
                var inheritanceLevel = 0;
                var currType = typeof(T);
                while (pi.DeclaringType != currType)
                {
                    inheritanceLevel++;
                    currType = currType.BaseType;
                }

                ColumnInfos.Add(new ColumnInfo
                {
                    Name = pi.Name,
                    DeclaringType = pi.DeclaringType,
                    Type = !isCollection ? pi.PropertyType.FullName : pi.PropertyType.GenericTypeArguments[0].FullName,
                    ForeignKeyPropName = fkName,
                    IsKey = false,
                    IsUnique = false,
                    IsNavigation = true,
                    IsCollection = isCollection,
                    IsNullable = false,
                    IsInherited = pi.DeclaringType != typeof(T),
                    InheritanceLevel = inheritanceLevel,
                    IsMtoN = isMtoN,
                    ReferencedTableName = isMtoN ? string.Join("", new[] { Name, pi.Name }.OrderBy(n => n)) : pi.Name
                });
                _propInfos.Add(new PropInfo
                {
                    Name = pi.Name,
                    PropertyType = pi.PropertyType,
                    CustomAttributes = pi.CustomAttributes,
                    IsNavigation = true,
                    FkName = fkName,
                    IsCollection = isCollection,
                    IsMtoN = isMtoN,
                    DeclaringType = pi.DeclaringType
                });
            }

            if (!hasKey) throw new EntityException("At least one primary key has to be declared.");
        }

        /// <summary>
        /// True, if the given property has been declared as virtual
        /// </summary>
        /// <param name="pi"></param>
        /// <returns></returns>
        private bool IsVirtual(PropertyInfo pi)
        {
            return pi.GetSetMethod().IsVirtual;
        }

        /// <summary>
        /// True, if the given property is mappable
        /// </summary>
        /// <param name="pi"></param>
        /// <returns></returns>
        private bool IsMappable(PropertyInfo pi)
        {
            return Mappings.Contain(pi.PropertyType) || IsNavigationProp(pi);
        }

        /// <summary>
        /// Ture if the given property is a navigation property
        /// </summary>
        /// <param name="pi"></param>
        /// <returns></returns>
        private bool IsNavigationProp(PropertyInfo pi)
        {
            return pi.GetGetMethod().IsVirtual && pi.GetSetMethod().IsVirtual;
            //&& _ctx.Tables.Any(t => t.GetType() == pi.PropertyType || (pi.PropertyType == typeof(ICollection<>) && t.GetType() == pi.PropertyType.GenericTypeArguments[0]));
        }
        #endregion

        public override string ToString()
        {
            var str = "";

            foreach (var e in _cache)
            {
                str += $"{e}\n";
            }

            return str;
        }
    }
}
