# SQL-Injection's Funeral
OR-Mapper Framework created for educational purposes based on .NET Core and Microsoft SQL Server.

*Further information you will find in "Doc/SQL Injection's Funeral.pdf"*
PLEASE READ THIS DOCUMENTATON BEFORE FIRST USAGE!


## CONNECTION STRINGS
To set or edit the connection strings in the Unit Test Project or in the PerformanceTest app just set the corresponding value in the static class "Configuration.cs" (one for each project).
The DLL path of the SampleModels project is also placed in the static class "Configuration.cs".

IMPORTANT: To use the OR-Mapper properly, assure that your connection string sets "MultipleActiveResultSets" (MARS) to TRUE!


## PERFORMANCE TEST APP

To run the included performance test app navigate to "PerformanceTest".
Then execute (in a CMD window) "dotnet run create" to create the tables.
Afterwards either run "dotnet run query" or "dotnet run query-verbose" depending on your desired output extent.

To set the DLL path and connection string value, navigate to "Configuration.cs" and recompile the project afterwards (using "dotnet build").



SQL-Injection's Funeral references the song "A Liar's Funeral" from the great metal band Slipknot.